package caixa;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import repsys.teste.com.repsys.CadastroSaqueCaixa;
import repsys.teste.com.repsys.R;
import sharedpreferences.ShHelper;
import usuario.Usuario;

public class TelaCaixa2 extends Fragment implements TelaCaixa {

    private FloatingActionButton fab;
    private List<Saque> saqueList;

    private ListView listaSaques;

    private CollectionReference saquesRef;

    private FirebaseFirestore firebaseFirestore;

    private ShHelper shHelper;




    @Override
    public void onStart() {
        super.onStart();

        saquesRef.get()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        saqueList.clear();

                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {

                                Saque saque = document.toObject(Saque.class);
                                saqueList.add(saque);
                            }

                            SaqueList saqueListAdapter = new SaqueList(getActivity(), saqueList);
                            listaSaques.setAdapter(saqueListAdapter);

                        } else {
                            Log.d("RETORNO: ", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_caixa2,
                container, false);


        listaSaques = (ListView) view.findViewById(R.id.lstCaixa2);

        saqueList = new ArrayList<>();


        shHelper = new ShHelper(getContext());

        Usuario usuario = shHelper.resgatarDadosUsuario();


        firebaseFirestore = FirebaseFirestore.getInstance();

        saquesRef = firebaseFirestore.collection("caixa").document(usuario.getIdRepublica()).collection("saques");


        listaSaques.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getContext(), saqueList.get(i).getMotivo(), Toast.LENGTH_SHORT).show();
                mostrarDetalhes(saqueList.get(i));
            }
        });

        fab = (FloatingActionButton) view.findViewById(R.id.fabSaqueCaixa);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CadastroSaqueCaixa.class);
                startActivity(intent);
            }
        });


        return view;
    }

    private void mostrarDetalhes(Saque saque){

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        String data = formato.format(saque.getDataSaque());


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.act_visualizar_detalhes_saque, null);
        dialogBuilder.setView(dialogView);

        TextView motivo = (TextView) dialogView.findViewById(R.id.txtdialogMotivoSaque);
        TextView descricao = (TextView) dialogView.findViewById(R.id.txtdialogDescricaoSaque);
        TextView dataSaque = (TextView) dialogView.findViewById(R.id.txtdialogDataSaque);
        TextView valorSaque = (TextView) dialogView.findViewById(R.id.txtdialogValorSaque);
        TextView valorAntes = (TextView) dialogView.findViewById(R.id.txtdialogValorAntes);
        TextView valorDepois = (TextView) dialogView.findViewById(R.id.txtdialogValorDepois);

        motivo.setText("Motivo: " + saque.getMotivo());
        descricao.setText("Descriçõa: " + saque.getDescricao());
        dataSaque.setText("Data do Saque: " + data);
        valorSaque.setText("Valor Saque: " + saque.getValorSacado());
        valorAntes.setText("Valor antes do saque: " + saque.getValorPreSaque());
        valorDepois.setText("Valor depois do saque: " + saque.getValorPosSaque());

        dialogBuilder.setTitle("Detalhes");
        final AlertDialog b = dialogBuilder.create();
        b.show();
    }
}
