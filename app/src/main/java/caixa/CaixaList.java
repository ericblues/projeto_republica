package caixa;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import repsys.teste.com.repsys.R;

public class CaixaList extends ArrayAdapter<Caixa> {
    private Activity context;
    List<Caixa> caixaList;

    public CaixaList (Activity context, List<Caixa> caixaList){

        super(context, R.layout.layout_caixa_list, caixaList);
        this.context = context;
        this.caixaList = caixaList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.layout_caixa_list, null, true);

        TextView valor = (TextView) listViewItem.findViewById(R.id.txtValorCaixa);
        TextView contribuinte = (TextView) listViewItem.findViewById(R.id.txtContribuinte);
        TextView dataContribuicao = (TextView) listViewItem.findViewById(R.id.txtDataContribuicao);

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        Caixa caixa = caixaList.get(position);
        valor.setText("Valor: " + caixa.getValor().toString());
        contribuinte.setText("Contribuinte: " + caixa.getContribuinte());
        dataContribuicao.setText("Data: " + formato.format(caixa.getData()));

        return listViewItem;
    }
}
