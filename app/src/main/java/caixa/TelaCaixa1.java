package caixa;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;

import repsys.teste.com.repsys.CadastroDepositoCaixa;
import repsys.teste.com.repsys.R;
import repsys.teste.com.repsys.TelaPrincipal;
import sharedpreferences.ShHelper;
import usuario.Usuario;

public class TelaCaixa1 extends Fragment implements TelaCaixa {

    private List<Caixa> caixaList;
    private FloatingActionButton fab;

    private ListView listaContribuicoes;

    private CollectionReference depositosRef;

    private FirebaseFirestore firebaseFirestore;

    private ShHelper shHelper;


    @Override
    public void onStart() {
        super.onStart();

        depositosRef.get()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        caixaList.clear();

                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {

                                Caixa caixa = document.toObject(Caixa.class);
                                caixaList.add(caixa);
                            }

                            CaixaList caixaListAdapter = new CaixaList(getActivity(), caixaList);
                            listaContribuicoes.setAdapter(caixaListAdapter);

                        } else {
                            Log.d("RETORNO: ", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_caixa1,
                container, false);

        listaContribuicoes = (ListView) view.findViewById(R.id.lstCaixa1);
        fab = (FloatingActionButton) view.findViewById(R.id.fabDepositoCaixa);
        caixaList = new ArrayList<>();


        shHelper = new ShHelper(getContext());

        Usuario usuario = shHelper.resgatarDadosUsuario();


        firebaseFirestore = FirebaseFirestore.getInstance();
        depositosRef = firebaseFirestore.collection("caixa").document(usuario.getIdRepublica()).collection("depositos");


/*
        for (int i = 0; i <= 10; i++){

            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            Date data = null;
            try {
                data = formato.parse("23/11/2015");
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Double valor = 10.0;

            Caixa caixaItem = new Caixa("","", "morador: " + i, valor, data);

            caixaList.add(caixaItem);
        }*/

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CadastroDepositoCaixa.class);
                startActivity(intent);
            }
        });

        //CaixaList caixaListAdapter = new CaixaList(getActivity(), caixaList);
        //listaContribuicoes.setAdapter(caixaListAdapter);


        return view;
    }
}
