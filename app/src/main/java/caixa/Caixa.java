package caixa;

import java.util.Date;

public class Caixa {


    private String idRepublica;
    private String idContribuinte;
    private String contribuinte;
    private Double valor;
    private Date data;

    public Caixa (){}

    public Caixa (String idRepublica, String idContribuinte, String contribuinte, Double valor, Date data){

        this.idRepublica = idRepublica;
        this.idContribuinte = idContribuinte;
        this.contribuinte = contribuinte;
        this.valor = valor;
        this.data = data;
    }


    public String getIdRepublica() {
        return idRepublica;
    }

    public void setIdRepublica(String idRepublica) {
        this.idRepublica = idRepublica;
    }

    public String getIdContribuinte() {
        return idContribuinte;
    }

    public void setIdContribuinte(String idContribuinte) {
        this.idContribuinte = idContribuinte;
    }

    public String getContribuinte() {
        return contribuinte;
    }

    public void setContribuinte(String contribuinte) {
        this.contribuinte = contribuinte;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
