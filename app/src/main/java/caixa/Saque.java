package caixa;

import java.util.Date;

public class Saque {


    private String idSaque;
    private String idRepublica;
    private String motivo;
    private String descricao;
    private Double valorSacado;
    private Double valorPreSaque;
    private Double valorPosSaque;
    private Date dataSaque;

    public Saque(){}

    public Saque(String idSaque, String idRepublica, String motivo, String descricao, Double valorSacado,
                 Double valorPreSaque, Double valorPosSaque, Date dataSaque){
        this.idSaque = idSaque;
        this.idRepublica = idRepublica;
        this.motivo = motivo;
        this.descricao = descricao;
        this.valorSacado = valorSacado;
        this.valorPreSaque = valorPreSaque;
        this.valorPosSaque = valorPosSaque;
        this.dataSaque = dataSaque;
    }

    public String getIdSaque() {
        return idSaque;
    }

    public void setIdSaque(String idSaque) {
        this.idSaque = idSaque;
    }

    public String getIdRepublica() {
        return idRepublica;
    }

    public void setIdRepublica(String idRepublica) {
        this.idRepublica = idRepublica;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValorSacado() {
        return valorSacado;
    }

    public void setValorSacado(Double valorSacado) {
        this.valorSacado = valorSacado;
    }

    public Double getValorPreSaque() {
        return valorPreSaque;
    }

    public void setValorPreSaque(Double valorPreSaque) {
        this.valorPreSaque = valorPreSaque;
    }

    public Double getValorPosSaque() {
        return valorPosSaque;
    }

    public void setValorPosSaque(Double valorPosSaque) {
        this.valorPosSaque = valorPosSaque;
    }

    public Date getDataSaque() {
        return dataSaque;
    }

    public void setDataSaque(Date dataSaque) {
        this.dataSaque = dataSaque;
    }
}
