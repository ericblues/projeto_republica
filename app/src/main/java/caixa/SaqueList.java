package caixa;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import repsys.teste.com.repsys.R;

public class SaqueList extends ArrayAdapter<Saque>{
    private Activity context;
    List<Saque> saqueList;

    public SaqueList (Activity context, List<Saque> saqueList){

        super(context, R.layout.layout_saque_list, saqueList);
        this.context = context;
        this.saqueList = saqueList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.layout_saque_list, null, true);

        TextView valorSaque = (TextView) listViewItem.findViewById(R.id.txtValorSaque);
        TextView motivo = (TextView) listViewItem.findViewById(R.id.txtMotivoSaque);
        TextView dataSaque = (TextView) listViewItem.findViewById(R.id.txtDataSaque);

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        Saque saque = saqueList.get(position);
        valorSaque.setText("Valor Saque: " + saque.getValorSacado().toString());
        motivo.setText("Motivo: " + saque.getMotivo());
        dataSaque.setText("Data: " + formato.format(saque.getDataSaque()));

        return listViewItem;
    }

}
