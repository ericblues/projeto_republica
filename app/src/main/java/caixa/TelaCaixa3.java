package caixa;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.anychart.anychart.AnyChart;
import com.anychart.anychart.AnyChartView;
import com.anychart.anychart.Cartesian;
import com.anychart.anychart.CartesianSeriesColumn;
import com.anychart.anychart.DataEntry;
import com.anychart.anychart.EnumsAnchor;
import com.anychart.anychart.HoverMode;
import com.anychart.anychart.Pie;
import com.anychart.anychart.Position;
import com.anychart.anychart.TooltipPositionMode;
import com.anychart.anychart.ValueDataEntry;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import repsys.teste.com.repsys.ListaMeses;
import repsys.teste.com.repsys.R;
import repsys.teste.com.repsys.RelatorioCaixaContribuinte;
import repsys.teste.com.repsys.RelatorioCaixaDeposito;
import repsys.teste.com.repsys.RelatorioCaixaSaque;
import sharedpreferences.ShHelper;
import usuario.Usuario;

public class TelaCaixa3 extends Fragment implements TelaCaixa {



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_caixa3,
                container, false);

        Button btnGerarRelatorio = (Button) view.findViewById(R.id.btnGerarRelatorioCaixa);
        final Spinner spnRelatorioCaixa = (Spinner) view.findViewById(R.id.spnRelatorioCaixa);
        final EditText edtDataInicialCaixa = (EditText) view.findViewById(R.id.edtDataInicialCaixa);
        final EditText edtDataFinalCaixa = (EditText) view.findViewById(R.id.edtDataFinalCaixa);




        btnGerarRelatorio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spnRelatorioCaixa.getSelectedItem().toString().trim().equals("Depósito")){

                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

                    Date dataDe = null;
                    Date dataAte = null;

                    try {
                        dataDe = formato.parse(edtDataInicialCaixa.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    try {
                        dataAte = formato.parse(edtDataFinalCaixa.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }



                    try{
                        Intent intent = new Intent(getContext(), RelatorioCaixaDeposito.class);
                        intent.putExtra("dataDe", dataDe);
                        intent.putExtra("dataAte", dataAte);
                        startActivity(intent);
                    }catch (Exception e){
                        Log.i("Erro Tela", e.toString());
                    }
                }
                if (spnRelatorioCaixa.getSelectedItem().toString().trim().equals("Saque")){

                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

                    Date dataDe = null;
                    Date dataAte = null;

                    try {
                        dataDe = formato.parse(edtDataInicialCaixa.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    try {
                        dataAte = formato.parse(edtDataFinalCaixa.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Intent intent = new Intent(view.getContext(), RelatorioCaixaSaque.class);
                    intent.putExtra("dataDe", dataDe);
                    intent.putExtra("dataAte", dataAte);
                    startActivity(intent);
                }
                if (spnRelatorioCaixa.getSelectedItem().toString().trim().equals("Contribuintes")){
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

                    Date dataDe = null;
                    Date dataAte = null;

                    try {
                        dataDe = formato.parse(edtDataInicialCaixa.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    try {
                        dataAte = formato.parse(edtDataFinalCaixa.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    Intent intent = new Intent(getContext(), RelatorioCaixaContribuinte.class);
                    intent.putExtra("dataDe", dataDe);
                    intent.putExtra("dataAte", dataAte);
                    startActivity(intent);
                }

            }
        });

        return view;
    }

}
