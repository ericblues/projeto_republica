package notificacao;

public class Solicitacao {

    private String idSolicitacao;
    private String idRepublica;
    private String idAdministrador;
    private String idSolicitante;
    private String nomeSolicitante;
    private Boolean resposta;
    private Boolean situacaoSolicitacao;
    private String urlFoto;

    public Solicitacao(){}

    public Solicitacao(String idSolicitacao, String idRepublica, String idAdministrador, String idSolicitante,
                       String nomeSolicitante, Boolean resposta, Boolean situacaoSolicitacao, String urlFoto){

        this.idSolicitacao = idSolicitacao;
        this.idRepublica = idRepublica;
        this.idAdministrador = idAdministrador;
        this.idSolicitante = idSolicitante;
        this.nomeSolicitante = nomeSolicitante;
        this.resposta = resposta;
        this.situacaoSolicitacao = situacaoSolicitacao;
        this.urlFoto = urlFoto;
    }

    public String getIdSolicitacao() {
        return idSolicitacao;
    }

    public void setIdSolicitacao(String idSolicitacao) {
        this.idSolicitacao = idSolicitacao;
    }

    public String getIdRepublica() {
        return idRepublica;
    }

    public void setIdRepublica(String idRepublica) {
        this.idRepublica = idRepublica;
    }

    public String getIdAdministrador() {
        return idAdministrador;
    }

    public void setIdAdministrador(String idAdministrador) {
        this.idAdministrador = idAdministrador;
    }

    public String getIdSolicitante() {
        return idSolicitante;
    }

    public void setIdSolicitante(String idSolicitante) {
        this.idSolicitante = idSolicitante;
    }

    public String getNomeSolicitante() {
        return nomeSolicitante;
    }

    public void setNomeSolicitante(String nomeSolicitante) {
        this.nomeSolicitante = nomeSolicitante;
    }

    public Boolean getResposta() {
        return resposta;
    }

    public void setResposta(Boolean resposta) {
        this.resposta = resposta;
    }

    public Boolean getSituacaoSolicitacao() {
        return situacaoSolicitacao;
    }

    public void setSituacaoSolicitacao(Boolean situacaoSolicitacao) {
        this.situacaoSolicitacao = situacaoSolicitacao;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }
}
