package notificacao;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import repsys.teste.com.repsys.R;

public class SolicitacaoRecebidaList extends ArrayAdapter<Solicitacao>{

    private Activity context;
    List<Solicitacao> solicitacaoList;

    public SolicitacaoRecebidaList(Activity context, List<Solicitacao> solicitacaoList){
        super (context, R.layout.layout_solicitacao_list, solicitacaoList);
        this.context = context;
        this.solicitacaoList = solicitacaoList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.layout_solicitacao_list, null, true);


        CircleImageView imgSolicitante = (CircleImageView) listViewItem.findViewById(R.id.imgSolicitante);
        TextView nomeSolicitante = (TextView) listViewItem.findViewById(R.id.txtNomeSolicitante);
        TextView situacaoSolicitacao = (TextView) listViewItem.findViewById(R.id.txtSituacaoSolicitacao);


        Solicitacao solicitacao = solicitacaoList.get(position);

        if (!solicitacao.getUrlFoto().isEmpty()){
            Glide.with(imgSolicitante.getContext())
                    .load(solicitacao.getUrlFoto())
                    .into(imgSolicitante);
        }

        nomeSolicitante.setText(solicitacao.getNomeSolicitante());

        if (!solicitacao.getSituacaoSolicitacao()){
            situacaoSolicitacao.setText("Situação: Pendente");
        }


        return listViewItem;
    }
}
