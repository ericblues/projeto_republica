package republica;


import java.io.Serializable;

public class Imovel implements Serializable {

    private String tipoImovel;
    private boolean internet;
    private boolean estacionamento;
    private int qtdeQuartos;
    private int qtdeBanheiros;
    private  int n_suite;
    private int n_quartosCompartilhados;

    public Imovel(){}

    public Imovel(String tipoImovel, boolean internet, boolean estacionamento, int qtdeQuartos, int qtdeBanheiros, int n_suite, int n_quartosCompartilhados){

        this.tipoImovel = tipoImovel;
        this.internet = internet;
        this.estacionamento = estacionamento;
        this.qtdeQuartos = qtdeQuartos;
        this.qtdeBanheiros = qtdeBanheiros;
        this.n_suite = n_suite;
        this.n_quartosCompartilhados = n_quartosCompartilhados;
    }

    public String getTipoImovel() {
        return tipoImovel;
    }

    public void setTipoImovel(String tipoImovel) {
        this.tipoImovel = tipoImovel;
    }

    public boolean isInternet() {
        return internet;
    }

    public void setInternet(boolean internet) {
        this.internet = internet;
    }

    public boolean isEstacionamento() {
        return estacionamento;
    }

    public void setEstacionamento(boolean estacionamento) {
        this.estacionamento = estacionamento;
    }

    public int getQtdeQuartos() {
        return qtdeQuartos;
    }

    public void setQtdeQuartos(int qtdeQuartos) {
        this.qtdeQuartos = qtdeQuartos;
    }

    public int getQtdeBanheiros() {
        return qtdeBanheiros;
    }

    public void setQtdeBanheiros(int qtdeBanheiros) {
        this.qtdeBanheiros = qtdeBanheiros;
    }

    public int getN_suite() {
        return n_suite;
    }

    public void setN_suite(int n_suite) {
        this.n_suite = n_suite;
    }

    public int getN_quartosCompartilhados() {
        return n_quartosCompartilhados;
    }

    public void setN_quartosCompartilhados(int n_quartosCompartilhados) {
        this.n_quartosCompartilhados = n_quartosCompartilhados;
    }
}
