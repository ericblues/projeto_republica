package republica;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import repsys.teste.com.repsys.R;
import sharedpreferences.ShHelper;

public class TelaVerRepublica2 extends Fragment implements TelaVerRepublica {


    private ShHelper shHelper;
    private Endereco endereco;

    private TextView txtCEP;
    private TextView txtRua;
    private TextView txtNumero;
    private TextView txtBairro;
    private TextView txtComplemento;
    private TextView txtCidade;
    private TextView txtEstado;

    @Override
    public void onStart() {
        super.onStart();


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ver_republica2,
                container, false);

        shHelper = new ShHelper(getContext());
        endereco = shHelper.resgatarDadosEndereco();

        txtCEP = (TextView) view.findViewById(R.id.txtConsultaCEP);
        txtRua = (TextView) view.findViewById(R.id.txtConsultaRua);
        txtNumero = (TextView) view.findViewById(R.id.txtConsultaNumero);
        txtBairro = (TextView) view.findViewById(R.id.txtConsultaBairro);
        txtComplemento = (TextView) view.findViewById(R.id.txtConsultaComplemento);
        txtCidade = (TextView) view.findViewById(R.id.txtConsultaCidade);
        txtEstado = (TextView) view.findViewById(R.id.txtConsultaEstado);

        try{
            txtCEP.setText("CEP: " + endereco.getCep());
            txtRua.setText("Rua: " + endereco.getRua());
            txtNumero.setText("Número: " + endereco.getNumero());
            txtBairro.setText("Bairro: " + endereco.getBairro());
            txtComplemento.setText("Complemento: " + endereco.getComplemento());
            txtCidade.setText("Cidade: " + endereco.getCidade());
            txtEstado.setText("Estado: " + endereco.getEstado());
        }catch (Exception e){
            Log.i("Erro consulta: ", e.toString());
            Toast.makeText(getContext(), "Não foi possível carregar o endereço, tente novamente", Toast.LENGTH_SHORT).show();
        }




        return view;
    }
}

