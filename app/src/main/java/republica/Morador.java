package republica;

import java.util.Date;

public class Morador {


    private String idMorador;
    private String idRepublica;
    private String idUsuario;
    private String nomeMorador;
    private String urlFoto;
    private Date dataEntrada;

    public Morador(){}

    public Morador(String idMorador, String idRepublica, String idUsuario, String nomeMorador, String urlFoto, Date dataEntrada){

        this.idMorador = idMorador;
        this.idRepublica = idRepublica;
        this.idUsuario = idUsuario;
        this.nomeMorador = nomeMorador;
        this.urlFoto = urlFoto;
        this.dataEntrada = dataEntrada;
    }

    public String getIdMorador() {
        return idMorador;
    }

    public void setIdMorador(String idMorador) {
        this.idMorador = idMorador;
    }

    public String getIdRepublica() {
        return idRepublica;
    }

    public void setIdRepublica(String idRepublica) {
        this.idRepublica = idRepublica;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNomeMorador() {
        return nomeMorador;
    }

    public void setNomeMorador(String nomeMorador) {
        this.nomeMorador = nomeMorador;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public Date getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(Date dataEntrada) {
        this.dataEntrada = dataEntrada;
    }
}
