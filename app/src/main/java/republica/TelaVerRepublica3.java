package republica;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import repsys.teste.com.repsys.R;
import sharedpreferences.ShHelper;
import usuario.Usuario;

public class TelaVerRepublica3 extends Fragment implements TelaVerRepublica {



    private ImageView fotoAnunciante;
    private TextView nomeAnunciante;
    private TextView emailAnunciante;
    private TextView telefoneAnunciante;


    private ShHelper shHelper;
    private Usuario anunciante;

    @Override
    public void onStart() {
        super.onStart();


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ver_republica3,
                container, false);

        fotoAnunciante = (ImageView) view.findViewById(R.id.foto_usuario_anunciante);
        nomeAnunciante = (TextView) view.findViewById(R.id.txtNomeAnunciante);
        emailAnunciante = (TextView) view.findViewById(R.id.txtEmailAnunciante);
        telefoneAnunciante = (TextView) view.findViewById(R.id.txtTelefoneAnunciante);

        shHelper = new ShHelper(getContext());
        anunciante = shHelper.resgatarDadosAnunciante();

        try {

            if (!anunciante.getFotoUrl().isEmpty()){
                Glide.with(fotoAnunciante.getContext())
                        .load(anunciante.getFotoUrl())
                        .into(fotoAnunciante);
            }

            nomeAnunciante.setText(anunciante.getNome());
            emailAnunciante.setText(anunciante.getEmail());
            telefoneAnunciante.setText(anunciante.getTelefone());
        }catch (Exception e){
            Log.i("Erro consulta: ", e.toString());
            Toast.makeText(getContext(), "Não foi possível carregar os dados para contato, tente novamente", Toast.LENGTH_SHORT).show();
        }
        return view;
    }
}

