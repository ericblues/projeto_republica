package republica;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.List;

import repsys.teste.com.repsys.R;

public class RepublicaList extends RecyclerView.Adapter<RepublicaList.ViewHolder>{

    private Context context;
    List<Republica> republicas;
    CustomItemClickListener listener;

    public RepublicaList(Context context, List<Republica> republicas){
        this.context = context;
        this.republicas = republicas;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_republica_card, parent, false);

        final ViewHolder mViewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mViewHolder.getPosition());
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {



        Republica republica = republicas.get(position);
        holder.txtViewName.setText(republica.getNomeRep());
        holder.txtViewRating.setText(String.valueOf(republica.getValorAluguel()));


        if (!republica.getUrlFoto().isEmpty()){
            Glide.with(holder.imgImagem.getContext())
                    .load(republica.getUrlFoto())
                    .into(holder.imgImagem);
        }
    }

    @Override
    public int getItemCount() {
        return 0;
    }



    public class ViewHolder extends RecyclerView.ViewHolder{


        public TextView txtViewName;
        public TextView txtViewRating;
        public ImageView imgImagem;

        public ViewHolder(View itemView) {

            super(itemView);


            txtViewName = (TextView) itemView.findViewById(R.id.txtViewName);
            txtViewRating = (TextView) itemView.findViewById(R.id.txtViewGenre);
            imgImagem = (ImageView) itemView.findViewById(R.id.imgImagem);
        }

    }
}
