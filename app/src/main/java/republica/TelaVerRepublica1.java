package republica;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import despesas.Despesa;
import despesas.DespesaList;
import despesas.TelaDespesa;
import repsys.teste.com.repsys.CadastroAtribuicaoDespesa;
import repsys.teste.com.repsys.CadastroDespesa;
import repsys.teste.com.repsys.R;
import sharedpreferences.ShHelper;
import usuario.Usuario;

public class TelaVerRepublica1 extends Fragment implements TelaVerRepublica {


    private ShHelper shHelper;
    private Republica republica;
    private Imovel imovel;
    private RestricoesRepublica restricoesRepublica;

    private ImageView imgRepublica;
    private TextView txtConsultaNomeRep;
    private TextView txtConsultaQtdeVagas;
    private TextView txtConsultaValorAluguel;
    private TextView txtConsultaContasInclusas;
    private TextView txtConsultaDescRep;
    private TextView txtConsultaTipoImovel;
    private TextView txtConsultaInternet;
    private TextView txtConsultaEstacionamento;
    private TextView txtConsultaQtdeQuartos;
    private TextView txtConsultaQtdeBanheiros;
    private TextView txtConsultaQtdeSuite;
    private TextView txtConsultaFumantes;
    private TextView txtConsultaAnimais;
    private TextView txtConsultaVisitas;



    @Override
    public void onStart() {
        super.onStart();


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState){

        View view = inflater.inflate(R.layout.fragment_ver_republica1,
                container, false);


        shHelper = new ShHelper(getContext());

        republica = shHelper.resgatarDadosRepublica();
        imovel = shHelper.resgatarDadosImovel();
        restricoesRepublica = shHelper.resgatarDadosRestricoes();

        Log.i("Nome da republica: ", republica.getNomeRep());
        Log.i("Aluguel: ", String.valueOf(republica.getValorAluguel()));


        imgRepublica = (ImageView) view.findViewById(R.id.imageViewConsultaRep);

        txtConsultaNomeRep = (TextView) view.findViewById(R.id.txtConsultaNomeRep);
        txtConsultaQtdeVagas = (TextView) view.findViewById(R.id.txtConsultaQtdeVagas);
        txtConsultaValorAluguel = (TextView) view.findViewById(R.id.txtConsultaValorAluguel);
        txtConsultaContasInclusas = (TextView) view.findViewById(R.id.txtConsultaContasInclusas);
        txtConsultaDescRep = (TextView) view.findViewById(R.id.txtConsultaDescRep);
        txtConsultaTipoImovel = (TextView) view.findViewById(R.id.txtConsultaTipoImovel);
        txtConsultaInternet = (TextView) view.findViewById(R.id.txtConsultaInternet);
        txtConsultaEstacionamento = (TextView) view.findViewById(R.id.txtConsultaEstacionamento);
        txtConsultaQtdeQuartos = (TextView) view.findViewById(R.id.txtConsultaQtdeQuartos);
        txtConsultaQtdeBanheiros = (TextView) view.findViewById(R.id.txtConsultaQtdeBanheiros);
        txtConsultaQtdeSuite = (TextView) view.findViewById(R.id.txtConsultaQtdeSuite);

        txtConsultaFumantes = (TextView) view.findViewById(R.id.txtConsultaFumantes);
        txtConsultaAnimais = (TextView) view.findViewById(R.id.txtConsultaAnimais);
        txtConsultaVisitas = (TextView) view.findViewById(R.id.txtConsultaVisitas);


        try{

            if (!republica.getUrlFoto().isEmpty()){
                Glide.with(imgRepublica.getContext())
                        .load(republica.getUrlFoto())
                        .into(imgRepublica);
            }

            txtConsultaNomeRep.setText(republica.getNomeRep());
            txtConsultaQtdeVagas.setText("Vagas: " + republica.getQtdeVagas());
            txtConsultaValorAluguel.setText("Aluguel: " + republica.getValorAluguel());

            if (republica.isContasInclusas())
                txtConsultaContasInclusas.setText("Contas inclusas: Sim");
            else
                txtConsultaContasInclusas.setText("Contas inclusas: Não");

            txtConsultaTipoImovel.setText("Imóvel: " + imovel.getTipoImovel());


            if (imovel.isInternet())
                txtConsultaInternet.setText("Internet: Sim");
            else
                txtConsultaInternet.setText("Internet: Não");

            if (imovel.isEstacionamento())
                txtConsultaEstacionamento.setText("Estacionamento: Sim");
            else
                txtConsultaEstacionamento.setText("Estacionamento: Não");

            txtConsultaQtdeQuartos.setText("Qtde Quartos: " + imovel.getQtdeQuartos());
            txtConsultaQtdeBanheiros.setText("Qtde Banheiro: " + imovel.getQtdeBanheiros());
            txtConsultaQtdeSuite.setText("Qtde Suítes: " + imovel.getN_suite());

            if (restricoesRepublica.isFumantes())
                txtConsultaFumantes.setText("Aceita Fumantes: Sim");
            else
                txtConsultaFumantes.setText("Aceita Fumantes: Não");

            if (restricoesRepublica.isAnimais())
                txtConsultaAnimais.setText("Permite Animais: Sim");
            else
                txtConsultaAnimais.setText("Permite Animais: Não");

            if (restricoesRepublica.isVisita())
                txtConsultaVisitas.setText("Permite Visitantes: Sim");
            else
                txtConsultaVisitas.setText("Permite Visitantes: Não");

        }catch (Exception e){
            Toast.makeText(getContext(), "Erro ao consultar republica, tente novamente", Toast.LENGTH_SHORT).show();
            Log.i("Erro consulta: ", e.toString());
        }

        return view;
    }

}

