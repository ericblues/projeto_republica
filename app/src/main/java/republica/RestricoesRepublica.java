package republica;

import java.io.Serializable;

public class RestricoesRepublica implements Serializable {

    private boolean fumantes;
    private boolean animais;
    private boolean visita;

    public RestricoesRepublica(){}

    public RestricoesRepublica( boolean fumantes, boolean animais, boolean visita){
        this.fumantes = fumantes;
        this.animais = animais;
        this.visita = visita;
    }

    public boolean isFumantes() {
        return fumantes;
    }

    public void setFumantes(boolean fumantes) {
        this.fumantes = fumantes;
    }

    public boolean isAnimais() {
        return animais;
    }

    public void setAnimais(boolean animais) {
        this.animais = animais;
    }

    public boolean isVisita() {
        return visita;
    }

    public void setVisita(boolean visita) {
        this.visita = visita;
    }
}
