package republica;

import java.io.Serializable;
import java.util.Map;

public class Republica implements Serializable{


    private String idRepublica;
    private String idAdministrador;
    private String nomeRep;
    private double valorAluguel;
    private boolean contasInclusas;
    private int qtdeVagas;
    private String tipoRepublica;
    private String urlFoto;
    private Double valorCaixa;
    private Map<String, Object> indicesMap;

    public Republica(){

    }

    public Republica(String idRepublica, String idAdministrador, String nomeRep,
                     double valorAluguel, boolean contasInclusas, int qtdeVagas,
                     String tipoRepublica, String urlFoto, Double valorCaixa, Map<String, Object> indicesMap){

        this.idRepublica = idRepublica;
        this.idAdministrador = idAdministrador;
        this.nomeRep = nomeRep;
        this.valorAluguel = valorAluguel;
        this.contasInclusas = contasInclusas;
        this.qtdeVagas = qtdeVagas;
        this.tipoRepublica = tipoRepublica;
        this.urlFoto = urlFoto;
        this.valorCaixa = valorCaixa;
        this.indicesMap = indicesMap;

    }

    public String getIdRepublica() {
        return idRepublica;
    }

    public void setIdRepublica(String idRepublica) {
        this.idRepublica = idRepublica;
    }

    public String getIdAdministrador() {
        return idAdministrador;
    }

    public void setIdAdministrador(String idAdministrador) {
        this.idAdministrador = idAdministrador;
    }

    public String getNomeRep() {
        return nomeRep;
    }

    public void setNomeRep(String nomeRep) {
        this.nomeRep = nomeRep;
    }

    public double getValorAluguel() {
        return valorAluguel;
    }

    public void setValorAluguel(double valorAluguel) {
        this.valorAluguel = valorAluguel;
    }

    public boolean isContasInclusas() {
        return contasInclusas;
    }

    public void setContasInclusas(boolean contasInclusas) {
        this.contasInclusas = contasInclusas;
    }

    public int getQtdeVagas() {
        return qtdeVagas;
    }

    public void setQtdeVagas(int qtdeVagas) {
        this.qtdeVagas = qtdeVagas;
    }

    public String getTipoRepublica() {
        return tipoRepublica;
    }

    public void setTipoRepublica(String tipoRepublica) {
        this.tipoRepublica = tipoRepublica;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public Double getValorCaixa() {
        return valorCaixa;
    }

    public void setValorCaixa(Double valorCaixa) {
        this.valorCaixa = valorCaixa;
    }

    public Map<String, Object> getIndicesMap() {
        return indicesMap;
    }

    public void setIndicesMap(Map<String, Object> indicesMap) {
        this.indicesMap = indicesMap;
    }
}
