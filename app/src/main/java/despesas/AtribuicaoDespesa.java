package despesas;

import java.util.Date;

public class AtribuicaoDespesa {

    private String idAtribuicao;
    private String idDespesa;
    private String idUsuario;
    private String tituloDespesa;
    private String tipoDespesa;
    private String nomeMorador;
    private Double valorPagar;
    private Double valorPago;
    private Date dataVencimento;
    private Date dataPagamento;
    private String urlFotoComprovante;
    private boolean situacaoPagamento;


    public AtribuicaoDespesa(){}

    public AtribuicaoDespesa(String idAtribuicao, String idDespesa, String idUsuario, String tituloDespesa, String tipoDespesa, String nomeMorador, Double valorPagar, Double valorPago,
                             Date dataVencimento, Date dataPagamento, String urlFotoComprovante, boolean situacaoPagamento){

        this.idAtribuicao = idAtribuicao;
        this.idDespesa = idDespesa;
        this.idUsuario = idUsuario;
        this.tituloDespesa = tituloDespesa;
        this.tipoDespesa = tipoDespesa;
        this.nomeMorador = nomeMorador;
        this.valorPagar = valorPagar;
        this.valorPago = valorPago;
        this.dataVencimento = dataVencimento;
        this.dataPagamento = dataPagamento;
        this.urlFotoComprovante = urlFotoComprovante;
        this.situacaoPagamento = situacaoPagamento;

    }

    public String getIdAtribuicao() {
        return idAtribuicao;
    }

    public void setIdAtribuicao(String idAtribuicao) {
        this.idAtribuicao = idAtribuicao;
    }

    public String getIdDespesa() {
        return idDespesa;
    }

    public void setIdDespesa(String idDespesa) {
        this.idDespesa = idDespesa;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }


    public String getTituloDespesa() {
        return tituloDespesa;
    }

    public void setTituloDespesa(String tituloDespesa) {
        this.tituloDespesa = tituloDespesa;
    }

    public String getTipoDespesa() {
        return tipoDespesa;
    }

    public void setTipoDespesa(String tipoDespesa) {
        this.tipoDespesa = tipoDespesa;
    }

    public String getNomeMorador() {
        return nomeMorador;
    }

    public void setNomeMorador(String nomeMorador) {
        this.nomeMorador = nomeMorador;
    }

    public Double getValorPagar() {
        return valorPagar;
    }

    public void setValorPagar(Double valorPagar) {
        this.valorPagar = valorPagar;
    }

    public Double getValorPago() {
        return valorPago;
    }

    public void setValorPago(Double valorPago) {
        this.valorPago = valorPago;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public Date getDataPagamento() {
        return dataPagamento;
    }

    public void setDataPagamento(Date dataPagamento) {
        this.dataPagamento = dataPagamento;
    }

    public String getUrlFotoComprovante() {
        return urlFotoComprovante;
    }

    public void setUrlFotoComprovante(String urlFotoComprovante) {
        this.urlFotoComprovante = urlFotoComprovante;
    }

    public boolean isSituacaoPagamento() {
        return situacaoPagamento;
    }

    public void setSituacaoPagamento(boolean situacaoPagamento) {
        this.situacaoPagamento = situacaoPagamento;
    }
}
