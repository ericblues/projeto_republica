package despesas;

import java.io.Serializable;
import java.util.Date;

public class Despesa implements Serializable{

    private String idDespesa;
    private String idRepublica;
    private String tituloDespesa;
    private Double valorTotalDespesa;
    private Double valorRestanteDespesa;
    private Double valorRecebidoDespesa;
    private String tipoDespesa;
    private Date dataVencimento;
    private boolean situacaoPagamento;
    private boolean despesaMensal;


    public Despesa(){}

    public Despesa(String idDespesa, String idRepublica, String tituloDespesa, Double valorTotalDespesa, Double valorRestanteDespesa,
                   Double valorRecebidoDespesa, String tipoDespesa, Date dataVencimento,
                   boolean situacaoPagamento, boolean despesaMensal){

        this.idDespesa = idDespesa;
        this.idRepublica = idRepublica;
        this.tituloDespesa = tituloDespesa;
        this.valorTotalDespesa = valorTotalDespesa;
        this.valorRestanteDespesa = valorRestanteDespesa;
        this.valorRecebidoDespesa = valorRecebidoDespesa;
        this.tipoDespesa = tipoDespesa;
        this.dataVencimento = dataVencimento;
        this.situacaoPagamento = situacaoPagamento;
        this.despesaMensal = despesaMensal;

    }

    public String getIdDespesa() {
        return idDespesa;
    }

    public void setIdDespesa(String idDespesa) {
        this.idDespesa = idDespesa;
    }

    public String getIdRepublica() {
        return idRepublica;
    }

    public void setIdRepublica(String idRepublica) {
        this.idRepublica = idRepublica;
    }

    public String getTituloDespesa() {
        return tituloDespesa;
    }

    public void setTituloDespesa(String tituloDespesa) {
        this.tituloDespesa = tituloDespesa;
    }

    public Double getValorTotalDespesa() {
        return valorTotalDespesa;
    }

    public void setValorTotalDespesa(Double valorTotalDespesa) {
        this.valorTotalDespesa = valorTotalDespesa;
    }

    public Double getValorRestanteDespesa() {
        return valorRestanteDespesa;
    }

    public void setValorRestanteDespesa(Double valorRestanteDespesa) {
        this.valorRestanteDespesa = valorRestanteDespesa;
    }

    public Double getValorRecebidoDespesa() {
        return valorRecebidoDespesa;
    }

    public void setValorRecebidoDespesa(Double valorRecebidoDespesa) {
        this.valorRecebidoDespesa = valorRecebidoDespesa;
    }

    public String getTipoDespesa() {
        return tipoDespesa;
    }

    public void setTipoDespesa(String tipoDespesa) {
        this.tipoDespesa = tipoDespesa;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public boolean isSituacaoPagamento() {
        return situacaoPagamento;
    }

    public void setSituacaoPagamento(boolean situacaoPagamento) {
        this.situacaoPagamento = situacaoPagamento;
    }

    public boolean isDespesaMensal() {
        return despesaMensal;
    }

    public void setDespesaMensal(boolean despesaMensal) {
        this.despesaMensal = despesaMensal;
    }
}
