package despesas;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import repsys.teste.com.repsys.R;

public class DespesaList extends ArrayAdapter<Despesa>{

    private Activity context;
    List<Despesa> despesaList;

    public DespesaList(Activity context, List<Despesa> despesaList){
        super(context, R.layout.layout_despesa_list, despesaList); //mudar essa linha antes de testar
        this.context = context;
        this.despesaList = despesaList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.layout_despesa_list, null, true); //mudar essa linha


        TextView tituloDespesa = (TextView) listViewItem.findViewById(R.id.txtTituloDespesa);
        TextView tipoDespesa = (TextView) listViewItem.findViewById(R.id.txtTipoDespesa);
        TextView valorDespesa = (TextView) listViewItem.findViewById(R.id.txtValorDespesa);
        TextView situacaoDespesa = (TextView) listViewItem.findViewById(R.id.txtSituacaoDespesa);

        Despesa despesa = despesaList.get(position);

        tituloDespesa.setText("Despesa: " + despesa.getTituloDespesa());
        tipoDespesa.setText("Tipo: " + despesa.getTipoDespesa());
        valorDespesa.setText("Valor: " + despesa.getValorTotalDespesa());

        if (despesa.isSituacaoPagamento())
            situacaoDespesa.setText("Situação: Pago");
        else
            situacaoDespesa.setText("Situação: Pendente");


        return listViewItem;
    }
}
