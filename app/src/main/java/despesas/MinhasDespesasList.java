package despesas;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import repsys.teste.com.repsys.R;

public class MinhasDespesasList extends ArrayAdapter<AtribuicaoDespesa>{

    private Activity context;
    List<AtribuicaoDespesa> atribuicaoDespesaList;

    public MinhasDespesasList(Activity context, List<AtribuicaoDespesa> atribuicaoDespesaList){
        super(context, R.layout.layout_minhas_despesas_list, atribuicaoDespesaList); //mudar essa linha antes de testar
        this.context = context;
        this.atribuicaoDespesaList = atribuicaoDespesaList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.layout_minhas_despesas_list, null, true); //mudar essa linha

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        TextView tituloDespesa = (TextView) listViewItem.findViewById(R.id.txtTituloMinhaDespesa);
        TextView valorDespesa = (TextView) listViewItem.findViewById(R.id.txtValorMinhaDespesa);
        TextView dataVencimento = (TextView) listViewItem.findViewById(R.id.txtDataVencimentoMinhaDespesa);
        TextView situacaoDespesa = (TextView) listViewItem.findViewById(R.id.txtSituacaoMinhaDespesa);

        AtribuicaoDespesa atribuicaoDespesa = atribuicaoDespesaList.get(position);

        tituloDespesa.setText("Despesa: " + atribuicaoDespesa.getTituloDespesa());
        valorDespesa.setText("Valor: R$" + atribuicaoDespesa.getValorPagar());
        dataVencimento.setText("Data Vencimento: " + formato.format(atribuicaoDespesa.getDataVencimento()));

        if (atribuicaoDespesa.isSituacaoPagamento())
            situacaoDespesa.setText("Situação: Pago");
        else
            situacaoDespesa.setText("Situação: Pendente");


        return listViewItem;
    }
}
