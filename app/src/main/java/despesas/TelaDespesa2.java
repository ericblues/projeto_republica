package despesas;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;

import repsys.teste.com.repsys.R;
import repsys.teste.com.repsys.VisualizarRecibo;
import repsys.teste.com.repsys.VisualizarRepublica;
import sharedpreferences.ShHelper;
import usuario.Usuario;

import static android.app.Activity.RESULT_OK;

public class TelaDespesa2 extends Fragment implements TelaDespesa {

    private List<AtribuicaoDespesa> minhasDespesasList;
    private ListView listaMinhasDespesas;


    private CollectionReference minhasDespesasRef;
    private FirebaseFirestore firebaseFirestore;
    private ShHelper shHelper;


    private Usuario usuario;
    private AtribuicaoDespesa minhaDespesa;
    private Despesa despesa;

    private AlertDialog alertDialog;


    private static final int RC_PHOTO_PICKER =  2;
    private FirebaseStorage mFirebaseStorage;
    private StorageReference mReciboPhotoStorageReference;
    //progress dialog
    private ProgressDialog progressDialog;

    private AlertDialog alertDialogFiltros;

    private int mYear, mMonth, mDay, mHour, mMinute;


    private FloatingActionMenu floatingActionMenu;
    private FloatingActionButton fabFiltrar;
    private FloatingActionButton fabLimparFiltros;



    @Override
    public void onStart() {
        super.onStart();

            minhasDespesasRef.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    minhasDespesasList.clear();

                    if (task.isSuccessful()){
                        for (DocumentSnapshot document: task.getResult()) {

                            AtribuicaoDespesa minhaDespesa = document.toObject(AtribuicaoDespesa.class);
                            minhasDespesasList.add(minhaDespesa);
                        }

                        try{
                            MinhasDespesasList minhasDespesasListAdapter = new MinhasDespesasList(getActivity(), minhasDespesasList);
                            listaMinhasDespesas.setAdapter(minhasDespesasListAdapter);
                        }catch (Exception e){
                            Toast.makeText(getContext(), "Não foi possível carregar os dados, tente novamente", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_despesa2,
                container, false);

        listaMinhasDespesas = (ListView) view.findViewById(R.id.lstMinhasDespesas);
        minhasDespesasList = new ArrayList<>();

        shHelper = new ShHelper(getContext());

        usuario = shHelper.resgatarDadosUsuario();

        firebaseFirestore = FirebaseFirestore.getInstance();

        mFirebaseStorage = FirebaseStorage.getInstance();


        minhasDespesasRef = firebaseFirestore.collection("despesa").document(usuario.getIdRepublica())
                .collection("atribuicoes").document(usuario.getUid()).collection("minhasAtribuicoes");


        floatingActionMenu = (FloatingActionMenu) view.findViewById(R.id.floatMenuMinhasDespesas);
        fabFiltrar = (FloatingActionButton) view.findViewById(R.id.fbFiltroMinhaDespesa);
        fabLimparFiltros = (FloatingActionButton) view.findViewById(R.id.fbLimparFiltroMinhaDespesa);

        listaMinhasDespesas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mostrarDetalhes(minhasDespesasList.get(i));
            }
        });


        fabFiltrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarFiltros();
            }
        });

        fabLimparFiltros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStart();
            }
        });

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        mReciboPhotoStorageReference = mFirebaseStorage.getReference().child("despesa/"+usuario.getIdRepublica()+"/"+minhaDespesa.getIdDespesa()+"/"+minhaDespesa.getIdAtribuicao());

        if (requestCode == RC_PHOTO_PICKER && resultCode == RESULT_OK){
            Uri selectedImageUri = data.getData();

            // Get a reference to store file at chat_photos/<FILENAME>
            StorageReference photoRef = mReciboPhotoStorageReference.child(selectedImageUri.getLastPathSegment());

            progressDialog.setMessage("Fazendo upload da foto, aguarde...");
            progressDialog.show();

            // Upload file to Firebase Storage
            photoRef.putFile(selectedImageUri).addOnSuccessListener(getActivity(), new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // When the image has successfully uploaded, we get its download URL

                    progressDialog.dismiss();

                    Uri downloadUrl = taskSnapshot.getDownloadUrl();

                    Toast.makeText(getContext(), "Comprovante adicionado com sucesso", Toast.LENGTH_SHORT).show();
                    try{
                        firebaseFirestore.collection("despesa").document(usuario.getIdRepublica())
                                .collection("atribuicoes").document(usuario.getUid()).collection("minhasAtribuicoes")
                        .document(minhaDespesa.getIdAtribuicao()).update("urlFotoComprovante", downloadUrl.toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d("Foto info: ", "DocumentSnapshot successfully updated!");
                            }
                        })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.w("Foto info:", "Error updating document", e);
                                    }
                                });
                        //mFirebaseDatabase.getReference("republicas").child(idRepublica + "/urlFoto").setValue(downloadUrl.toString());
                    }catch (Exception e){
                        Log.i("ERRO AO SUBIR A FOTO: ", e.toString());
                    }

                    alertDialog.dismiss();
                    onStart();
                }
            });
        }
    }

    private void mostrarFiltros(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.act_filtro_despesa, null);
        dialogBuilder.setView(dialogView);

        final Spinner spnTipoDespesaFiltro = (Spinner) dialogView.findViewById(R.id.spnFiltroTipoGasto);
        final Spinner spnSituacaoPagamento = (Spinner) dialogView.findViewById(R.id.spnFiltroSituacaoPagamento);

        final EditText dataInicialFiltro = (EditText) dialogView.findViewById(R.id.edtDataInicialDespesaFiltro);
        final EditText dataFinalFiltro = (EditText) dialogView.findViewById(R.id.edtDataFinalDespesaFiltro);
        ImageButton btnDataInicialFiltro = (ImageButton) dialogView.findViewById(R.id.btnDataInicialDespesaFiltro);
        ImageButton btnDataFinalFiltro = (ImageButton) dialogView.findViewById(R.id.btnDataFinalDespesaFiltro);
        Button aplicarFiltros = (Button) dialogView.findViewById(R.id.btnAplicarFiltrosDespesa);

        dataInicialFiltro.setFocusable(false);
        dataFinalFiltro.setFocusable(false);

        dialogBuilder.setTitle("Filtro de Busca");
        alertDialogFiltros = dialogBuilder.create();
        alertDialogFiltros.show();


        aplicarFiltros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                Date dataInicial = null;
                Date dataFinal = null;

                try {
                    dataInicial = formato.parse(dataInicialFiltro.getText().toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                try {
                    dataFinal = formato.parse(dataFinalFiltro.getText().toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String tipo = spnTipoDespesaFiltro.getSelectedItem().toString();
                String situacao = spnSituacaoPagamento.getSelectedItem().toString();

                filtrarMinhasDespesas(dataInicial, dataFinal, tipo, situacao);

            }
        });

        btnDataInicialFiltro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                dataInicialFiltro.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        btnDataFinalFiltro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                dataFinalFiltro.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

    }

    private void filtrarMinhasDespesas(Date dataDe, Date dataAte, String tipoDespesa, String situacao){


        Boolean situacaoPagamento;

        Query queryDespesa = null;

        if (!tipoDespesa.equals("Todos")){

            if (situacao.equals("Todos")){
                queryDespesa = minhasDespesasRef
                        .whereGreaterThanOrEqualTo("dataVencimento", dataDe)
                        .whereLessThanOrEqualTo("dataVencimento", dataAte)
                        .whereEqualTo("tipoDespesa", tipoDespesa);
            }

            if (situacao.equals("Pago")){
                situacaoPagamento = true;

                queryDespesa = minhasDespesasRef
                        .whereGreaterThanOrEqualTo("dataPagamento", dataDe)
                        .whereLessThanOrEqualTo("dataPagamento", dataAte)
                        .whereEqualTo("tipoDespesa", tipoDespesa)
                        .whereEqualTo("situacaoPagamento", situacaoPagamento);
            }

            if (situacao.equals("Pendente")){
                situacaoPagamento = false;

                queryDespesa = minhasDespesasRef
                        .whereGreaterThanOrEqualTo("dataVencimento", dataDe)
                        .whereLessThanOrEqualTo("dataVencimento", dataAte)
                        .whereEqualTo("tipoDespesa", tipoDespesa)
                        .whereEqualTo("situacaoPagamento", situacaoPagamento);
            }

        }else{

            if (situacao.equals("Todos")){
                queryDespesa = minhasDespesasRef
                        .whereGreaterThanOrEqualTo("dataVencimento", dataDe)
                        .whereLessThanOrEqualTo("dataVencimento", dataAte);
            }

            if (situacao.equals("Pago")){
                situacaoPagamento = true;

                queryDespesa = minhasDespesasRef
                        .whereGreaterThanOrEqualTo("dataPagamento", dataDe)
                        .whereLessThanOrEqualTo("dataPagamento", dataAte)
                        .whereEqualTo("situacaoPagamento", situacaoPagamento);
            }

            if (situacao.equals("Pendente")){
                situacaoPagamento = false;

                queryDespesa = minhasDespesasRef
                        .whereGreaterThanOrEqualTo("dataVencimento", dataDe)
                        .whereLessThanOrEqualTo("dataVencimento", dataAte)
                        .whereEqualTo("situacaoPagamento", situacaoPagamento);
            }

        }



        queryDespesa.get()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful() && !task.getResult().isEmpty()) {
                            minhasDespesasList.clear();
                            for (DocumentSnapshot document : task.getResult()) {

                                AtribuicaoDespesa minhaDespesa = document.toObject(AtribuicaoDespesa.class);
                                minhasDespesasList.add(minhaDespesa);

                            }
                            try{
                                MinhasDespesasList despesaListAdapter = new MinhasDespesasList(getActivity(), minhasDespesasList);
                                listaMinhasDespesas.setAdapter(despesaListAdapter);
                                alertDialogFiltros.dismiss();
                            }catch (Exception e){
                                Toast.makeText(getContext(), "Nenhuma despesa encontrada para os filtros aplicados", Toast.LENGTH_SHORT).show();
                                alertDialogFiltros.dismiss();
                            }


                        } else {
                            Log.d("RETORNO: ", "Error getting documents: ", task.getException());
                            Toast.makeText(getContext(), "Nenhuma despesa encontrada para os filtros aplicados", Toast.LENGTH_SHORT).show();
                            alertDialogFiltros.dismiss();
                        }
                    }
                });
    }

    private void mostrarDetalhes(final AtribuicaoDespesa despesaSelecionada){


        minhaDespesa = despesaSelecionada;

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.act_visualizar_detalhes_minha_despesa, null);
        dialogBuilder.setView(dialogView);

        TextView tituloDespesa = (TextView) dialogView.findViewById(R.id.txtdialogTituloDespesaPagar);
        TextView tipoDespesa = (TextView) dialogView.findViewById(R.id.txtdialogTipoDespesaPagar);
        TextView dataVencimento = (TextView) dialogView.findViewById(R.id.txtdialogDataPagar);
        TextView dataPago = (TextView) dialogView.findViewById(R.id.txtdialogDataPago);
        TextView valorPagar = (TextView) dialogView.findViewById(R.id.txtdialogValorTotalDespesaPagar);
        TextView situacaoPagamento = (TextView) dialogView.findViewById(R.id.txtdialogSituacaoDespesaPagar);
        Button btnPagamento = (Button) dialogView.findViewById(R.id.btnPagar);
        Button btnRecibo = (Button) dialogView.findViewById(R.id.btnRecibo);

        tituloDespesa.setText("Despesa: " + minhaDespesa.getTituloDespesa());
        tipoDespesa.setText("Tipo: " + minhaDespesa.getTipoDespesa());
        valorPagar.setText("Valor: R$ " + minhaDespesa.getValorPagar());

        if (minhaDespesa.isSituacaoPagamento())
            situacaoPagamento.setText("Situação: Pago");
        else
            situacaoPagamento.setText("Situação: Pendente");

        dataVencimento.setText("Data Vencimento: " + formato.format(minhaDespesa.getDataVencimento()));

        try{
            dataPago.setText("Data Pagamento: " + formato.format(minhaDespesa.getDataPagamento()));
        }catch (Exception e){
            Log.i("Info: ", "Data de pagamento ainda não cadastrada");
        }

        if (minhaDespesa.isSituacaoPagamento()){
            btnPagamento.setVisibility(View.GONE);
            btnRecibo.setVisibility(View.VISIBLE);
        }
        else{
            btnPagamento.setVisibility(View.VISIBLE);
            btnRecibo.setVisibility(View.GONE);
        }

        dialogBuilder.setTitle("Detalhes");
        alertDialog = dialogBuilder.create();
        alertDialog.show();

        btnPagamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                efetuarPagamento();
            }
        });

        btnRecibo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Inicializando o progress dialog
                progressDialog = new ProgressDialog(getContext());

                if (minhaDespesa.getUrlFotoComprovante().isEmpty()){
                    //Toast.makeText(getContext(), "Nenhum comprovante anexado", Toast.LENGTH_SHORT).show();

                    final AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                    alertDialog.setTitle("Anexar Comprovante");
                    alertDialog.setMessage("Nenhum comprovante foi anexado. Anexar foto do comprovante agora?");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Anexar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.setType("image/jpeg");
                            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                            startActivityForResult(Intent.createChooser(intent, "Complete action using"), RC_PHOTO_PICKER);


                        }
                    });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Agora não", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Toast.makeText(getContext(), "Você pode anexar o comprovante mais tarde.", Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                        }
                    });
                    alertDialog.show();


                }else{
                    //Toast.makeText(getContext(), "Comprovante: " + minhaDespesa.getUrlFotoComprovante(), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(getActivity(), VisualizarRecibo.class);
                    intent.putExtra("urlRecibo", minhaDespesa.getUrlFotoComprovante());
                    startActivity(intent);
                }
            }
        });


    }

    private void efetuarPagamento(){


        DocumentReference minhaDespesaRef = minhasDespesasRef.document(minhaDespesa.getIdAtribuicao());

        minhaDespesaRef.update("valorPago", minhaDespesa.getValorPagar()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                updateDataPagamento();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }

    private void updateDataPagamento(){

        Date dataSistema = Calendar.getInstance().getTime();

        Date dataPagamento = dataSistema;

        DocumentReference minhaDespesaRef = minhasDespesasRef.document(minhaDespesa.getIdAtribuicao());


        minhaDespesaRef.update("dataPagamento", dataPagamento).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                updateSituacaoPagamento();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });



    }

    private void updateSituacaoPagamento(){
        DocumentReference minhaDespesaRef = minhasDespesasRef.document(minhaDespesa.getIdAtribuicao());

        minhaDespesaRef.update("situacaoPagamento", true).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                carregarDadosDespesaGeral();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });

    }


    private void carregarDadosDespesaGeral(){

        DocumentReference despesaRef = firebaseFirestore.collection("despesa")
                .document(usuario.getIdRepublica())
                .collection("despesas")
                .document(minhaDespesa.getIdDespesa());


        despesaRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.exists()) {


                        despesa = document.toObject(Despesa.class);

                        updateValorRecebidoDespesa();

                    } else {
                        Log.d("Informação: ", "Problema ao consultar dados para a despesa: " + minhaDespesa.getIdDespesa());
                    }
                } else {
                    Log.d("Falha: ", "get failed with ", task.getException());
                }
            }
        });
    }

    private void updateValorRecebidoDespesa(){

        DocumentReference despesaRef = firebaseFirestore.collection("despesa")
                .document(usuario.getIdRepublica())
                .collection("despesas")
                .document(minhaDespesa.getIdDespesa());


        Double valorRecebidoAnterior = despesa.getValorRecebidoDespesa();
        final Double valorRecebidoAtual = valorRecebidoAnterior + minhaDespesa.getValorPagar();

        despesaRef.update("valorRecebidoDespesa", valorRecebidoAtual).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                Toast.makeText(getContext(), "Status do Pagamento Atualizado !", Toast.LENGTH_SHORT).show();

                if (despesa.getValorTotalDespesa().doubleValue() == valorRecebidoAtual && despesa.getValorRestanteDespesa().doubleValue() == 0.0){
                    Toast.makeText(getContext(), "A despesa foi completamente paga por todos, atualizando status da despesa geral ...", Toast.LENGTH_LONG).show();

                    updateStatusPagamentoDespesaGeral();
                }else{
                    mostrarDialogAnexarRecibo();
                    onStart();
                    alertDialog.dismiss();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });


    }


    private void updateStatusPagamentoDespesaGeral(){

        DocumentReference despesaRef = firebaseFirestore.collection("despesa")
                .document(usuario.getIdRepublica())
                .collection("despesas")
                .document(minhaDespesa.getIdDespesa());

        despesaRef.update("situacaoPagamento", true).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mostrarDialogAnexarRecibo();
                onStart();
                alertDialog.dismiss();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });

    }

    private void mostrarDialogAnexarRecibo(){

        //Inicializando o progress dialog
        progressDialog = new ProgressDialog(getContext());

        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
        alertDialog.setTitle("Anexar Comprovante");
        alertDialog.setMessage("Anexar foto do comprovante?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Anexar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getContext(), "Você selecionou a opção positiva", Toast.LENGTH_SHORT).show();


                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/jpeg");
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(Intent.createChooser(intent, "Complete action using"), RC_PHOTO_PICKER);


            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Agora não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getContext(), "Você selecionou a opção negativa", Toast.LENGTH_SHORT).show();
            }
        });
        alertDialog.show();
    }
}
