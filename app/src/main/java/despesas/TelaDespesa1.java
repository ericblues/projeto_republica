package despesas;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.github.clans.fab.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import repsys.teste.com.repsys.CadastroAtribuicaoDespesa;
import repsys.teste.com.repsys.CadastroDespesa;
import repsys.teste.com.repsys.R;
import sharedpreferences.ShHelper;
import usuario.Usuario;

public class TelaDespesa1 extends Fragment implements TelaDespesa {

    private int mYear, mMonth, mDay, mHour, mMinute;

    //private FloatingActionButton fab;

    private List<Despesa> despesaList;
    private ListView listaDespesas;


    private CollectionReference despesasRef;

    private FirebaseFirestore firebaseFirestore;
    private FloatingActionMenu floatingActionMenu;
    private FloatingActionButton fabAdicionar;
    private FloatingActionButton fabFiltrar;
    private FloatingActionButton fabLimparFiltros;

    private ShHelper shHelper;


    private AlertDialog alertDialogFiltros;

    @Override
    public void onStart() {
        super.onStart();

        despesasRef.get()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        despesaList.clear();

                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {

                                Despesa despesa = document.toObject(Despesa.class);
                                despesaList.add(despesa);
                            }

                            try{
                                DespesaList despesaListAdapter = new DespesaList(getActivity(), despesaList);
                                listaDespesas.setAdapter(despesaListAdapter);
                            }catch (Exception e){
                                Toast.makeText(getContext(), "Não foi possível carregar os dados, tente novamente", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Log.d("RETORNO: ", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_despesa1,
                container, false);


        listaDespesas = (ListView) view.findViewById(R.id.lstDespesa1);
        despesaList = new ArrayList<>();
        //fab = (FloatingActionButton) view.findViewById(R.id.fabAdicionarDespesa);

        floatingActionMenu = (FloatingActionMenu) view.findViewById(R.id.material_design_android_floating_action_menu);
        fabAdicionar = (FloatingActionButton) view.findViewById(R.id.fbAdicionarDespesa);
        fabFiltrar = (FloatingActionButton) view.findViewById(R.id.fbFiltroDespesa);
        fabLimparFiltros = (FloatingActionButton) view.findViewById(R.id.fbLimparFiltroDespesa);

        shHelper = new ShHelper(getContext());

        Usuario usuario = shHelper.resgatarDadosUsuario();

        firebaseFirestore = FirebaseFirestore.getInstance();


        despesasRef = firebaseFirestore.collection("despesa").document(usuario.getIdRepublica()).collection("despesas");

        listaDespesas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mostrarDetalhes(despesaList.get(i));
            }
        });

        fabAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CadastroDespesa.class);
                startActivity(intent);
            }
        });

        fabFiltrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarFiltros();
            }
        });

        fabLimparFiltros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStart();
            }
        });

        return view;
    }

    private void mostrarFiltros(){

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.act_filtro_despesa, null);
        dialogBuilder.setView(dialogView);

        final Spinner spnTipoDespesaFiltro = (Spinner) dialogView.findViewById(R.id.spnFiltroTipoGasto);
        final Spinner spnSituacaoPagamento = (Spinner) dialogView.findViewById(R.id.spnFiltroSituacaoPagamento);

        final EditText dataInicialFiltro = (EditText) dialogView.findViewById(R.id.edtDataInicialDespesaFiltro);
        final EditText dataFinalFiltro = (EditText) dialogView.findViewById(R.id.edtDataFinalDespesaFiltro);
        ImageButton btnDataInicialFiltro = (ImageButton) dialogView.findViewById(R.id.btnDataInicialDespesaFiltro);
        ImageButton btnDataFinalFiltro = (ImageButton) dialogView.findViewById(R.id.btnDataFinalDespesaFiltro);
        Button aplicarFiltros = (Button) dialogView.findViewById(R.id.btnAplicarFiltrosDespesa);

        dataInicialFiltro.setFocusable(false);
        dataFinalFiltro.setFocusable(false);

        dialogBuilder.setTitle("Filtro de Busca");
        alertDialogFiltros = dialogBuilder.create();
        alertDialogFiltros.show();

        aplicarFiltros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                Date dataInicial = null;
                Date dataFinal = null;

                try {
                    dataInicial = formato.parse(dataInicialFiltro.getText().toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                try {
                    dataFinal = formato.parse(dataFinalFiltro.getText().toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String tipo = spnTipoDespesaFiltro.getSelectedItem().toString();
                String situacao = spnSituacaoPagamento.getSelectedItem().toString();

                filtrarDespesas(dataInicial, dataFinal, tipo, situacao);

            }
        });

        btnDataInicialFiltro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                dataInicialFiltro.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        btnDataFinalFiltro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                dataFinalFiltro.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

    }


    private void filtrarDespesas(Date dataDe, Date dataAte, String tipoDespesa, String situacao){


        Boolean situacaoPagamento;

        Query queryDespesa = null;

        if (!tipoDespesa.equals("Todos")){

            if (situacao.equals("Todos")){
                queryDespesa = despesasRef
                        .whereGreaterThanOrEqualTo("dataVencimento", dataDe)
                        .whereLessThanOrEqualTo("dataVencimento", dataAte)
                        .whereEqualTo("tipoDespesa", tipoDespesa);
            }

            if (situacao.equals("Pago")){
                situacaoPagamento = true;

                queryDespesa = despesasRef
                        .whereGreaterThanOrEqualTo("dataVencimento", dataDe)
                        .whereLessThanOrEqualTo("dataVencimento", dataAte)
                        .whereEqualTo("tipoDespesa", tipoDespesa)
                        .whereEqualTo("situacaoPagamento", situacaoPagamento);
            }

            if (situacao.equals("Pendente")){
                situacaoPagamento = false;

                queryDespesa = despesasRef
                        .whereGreaterThanOrEqualTo("dataVencimento", dataDe)
                        .whereLessThanOrEqualTo("dataVencimento", dataAte)
                        .whereEqualTo("tipoDespesa", tipoDespesa)
                        .whereEqualTo("situacaoPagamento", situacaoPagamento);
            }

        }else{

            if (situacao.equals("Todos")){
                queryDespesa = despesasRef
                        .whereGreaterThanOrEqualTo("dataVencimento", dataDe)
                        .whereLessThanOrEqualTo("dataVencimento", dataAte);
            }

            if (situacao.equals("Pago")){
                situacaoPagamento = true;

                queryDespesa = despesasRef
                        .whereGreaterThanOrEqualTo("dataVencimento", dataDe)
                        .whereLessThanOrEqualTo("dataVencimento", dataAte)
                        .whereEqualTo("situacaoPagamento", situacaoPagamento);
            }

            if (situacao.equals("Pendente")){
                situacaoPagamento = false;

                queryDespesa = despesasRef
                        .whereGreaterThanOrEqualTo("dataVencimento", dataDe)
                        .whereLessThanOrEqualTo("dataVencimento", dataAte)
                        .whereEqualTo("situacaoPagamento", situacaoPagamento);
            }

        }



        queryDespesa.get()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful() && !task.getResult().isEmpty()) {
                            despesaList.clear();
                            for (DocumentSnapshot document : task.getResult()) {

                                Despesa despesa = document.toObject(Despesa.class);
                                despesaList.add(despesa);

                            }
                            try{
                                DespesaList despesaListAdapter = new DespesaList(getActivity(), despesaList);
                                listaDespesas.setAdapter(despesaListAdapter);
                                alertDialogFiltros.dismiss();
                            }catch (Exception e){
                                Toast.makeText(getContext(), "Nenhuma despesa encontrada para os filtros aplicados", Toast.LENGTH_SHORT).show();
                                alertDialogFiltros.dismiss();
                            }


                        } else {
                            Log.d("RETORNO: ", "Error getting documents: ", task.getException());
                            Toast.makeText(getContext(), "Nenhuma despesa encontrada para os filtros aplicados", Toast.LENGTH_SHORT).show();
                            alertDialogFiltros.dismiss();
                        }
                    }
                });
    }


    private void mostrarDetalhes(final Despesa despesa){
        //SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        //String data;

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.act_visualizar_detalhes_despesa, null);
        dialogBuilder.setView(dialogView);


        TextView titulo = dialogView.findViewById(R.id.txtdialogTituloDespesa);
        TextView tipo = dialogView.findViewById(R.id.txtdialogTipoDespesa);
        TextView valorTotal = dialogView.findViewById(R.id.txtdialogValorTotalDespesa);
        TextView faltaAtribuir = dialogView.findViewById(R.id.txtdialogFaltaAtribuir);
        TextView valorRecebido = dialogView.findViewById(R.id.txtdialogValorRecebidoDespesa);
        TextView situacao = dialogView.findViewById(R.id.txtdialogSituacaoDespesa);
        Button btnAtribuirDespesa = dialogView.findViewById(R.id.btnAtribuirDespesa);
        Button btnCancelarAtbVoltar = dialogView.findViewById(R.id.btnEditarListDespesas);


        titulo.setText("Despesa: " + despesa.getTituloDespesa());
        tipo.setText("Tipo: " + despesa.getTipoDespesa());
        valorTotal.setText("Valor total da despesa: R$ " + despesa.getValorTotalDespesa());
        faltaAtribuir.setText("Falta atribuir: R$ " + despesa.getValorRestanteDespesa());
        valorRecebido.setText("Valor recebido: R$ " + despesa.getValorRecebidoDespesa());

        if (despesa.isSituacaoPagamento())
            situacao.setText("Situação: Pago");
        else
            situacao.setText("Situação: Pendente");

        dialogBuilder.setTitle("Detalhes");
        final AlertDialog b = dialogBuilder.create();
        b.show();

        btnAtribuirDespesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!despesa.isSituacaoPagamento()){
                    Intent intent = new Intent(getActivity(), CadastroAtribuicaoDespesa.class);
                    intent.putExtra("despesa", despesa);
                    startActivity(intent);

                    b.dismiss();
                }else{
                    Toast.makeText(getContext(), "Despesa completamente paga pelos moradores!", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}
