package despesas;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import repsys.teste.com.repsys.R;
import repsys.teste.com.repsys.RelatorioDespesaTipoGasto;
import repsys.teste.com.repsys.RelatorioPagoPendentePessoal;
import repsys.teste.com.repsys.RelatorioPagoPendenteRepublica;

public class TelaDespesa3 extends Fragment implements TelaDespesa {


    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_despesa3,
                container, false);

        final EditText edtDataIncial = (EditText) view.findViewById(R.id.edtDataInicialDespesa);
        final EditText edtDataFinal =  (EditText) view.findViewById(R.id.edtDataFinalDespesa);

        ImageButton imgDataInicial = (ImageButton) view.findViewById(R.id.btnDataInicialDespesa);
        ImageButton imgDataFinal = (ImageButton) view.findViewById(R.id.btnDataFinalDespesa);

        final Spinner spnRelatorioDespesa = (Spinner) view.findViewById(R.id.spnRelatorioDespesa);

        Button btnGerarRelatorio = (Button) view.findViewById(R.id.btnGerarRelatorioDespesa);

        edtDataIncial.setFocusable(false);
        edtDataFinal.setFocusable(false);



        imgDataInicial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                edtDataIncial.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        imgDataFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                edtDataFinal.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        btnGerarRelatorio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                Date dataInicial = null;
                Date dataFinal = null;

                try {
                    dataInicial = formato.parse(edtDataIncial.getText().toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                try {
                    dataFinal = formato.parse(edtDataFinal.getText().toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (dataInicial.after(dataFinal)){
                    Toast.makeText(getContext(), "A data inicial não pode ser maior que a data final", Toast.LENGTH_SHORT).show();
                }else{

                    if (spnRelatorioDespesa.getSelectedItem().toString().equals("Tipos de Gastos")){
                        Intent intent = new Intent(getContext(), RelatorioDespesaTipoGasto.class);
                        intent.putExtra("dataDe", dataInicial);
                        intent.putExtra("dataAte", dataFinal);
                        startActivity(intent);
                    }
                    if (spnRelatorioDespesa.getSelectedItem().toString().equals("Pagos e Pendentes Pessoal")){
                        Intent intent = new Intent(getContext(), RelatorioPagoPendentePessoal.class);
                        intent.putExtra("dataDe", dataInicial);
                        intent.putExtra("dataAte", dataFinal);
                        startActivity(intent);
                    }
                    if (spnRelatorioDespesa.getSelectedItem().toString().equals("Pagos e Pendentes Republica")){
                        Intent intent = new Intent(getContext(), RelatorioPagoPendenteRepublica.class);
                        intent.putExtra("dataDe", dataInicial);
                        intent.putExtra("dataAte", dataFinal);
                        startActivity(intent);
                    }

                }
            }
        });

        return view;
    }
}
