package repsys.teste.com.repsys;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class PrimeiroAcessoFragment extends Fragment {


    //Botões
    private ImageButton btnIniciaCadastrar;
    private ImageButton btnIniciarPesquisa;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_primeiro_acesso,
                container, false);

        //Inicializando os botões
        btnIniciaCadastrar = (ImageButton) view.findViewById(R.id.btnInicialCadastrarRepublica);
        btnIniciarPesquisa = (ImageButton) view.findViewById(R.id.btnInicialProcurarRepublica);

        //Ações dos botões
        btnIniciaCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), "VOcê clicou", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(view.getContext(), CadastroRepublica.class);
                startActivity(intent);
            }
        });

        btnIniciarPesquisa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), PesquisaRepublica.class);
                startActivity(intent);
            }
        });
        return view;
    }
}
