package repsys.teste.com.repsys;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.Inflater;

import de.hdodenhof.circleimageview.CircleImageView;
import usuario.Usuario;

public class PerfilFragment extends Fragment{


    private TextView txtNome;
    private TextView txtEmail;
    private TextView txtTelefone;
    private CircleImageView foto_Usuario;

    //Firebase Instance Variables
    private FirebaseAuth mFirebaseAuth;
    private FirebaseFirestore firebaseFirestore;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_perfil,
                container, false);

        //Inicializando componentes do firebase
        mFirebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        txtNome = (TextView) view.findViewById(R.id.txtUserName);
        txtEmail = (TextView) view.findViewById(R.id.txtUserEmail);
        txtTelefone = (TextView) view.findViewById(R.id.txtUserPhone);
        foto_Usuario = (CircleImageView) view.findViewById(R.id.foto_usuario);

        return view;
    }

    private void preencheDados(Usuario usuario){
        txtNome.setText(usuario.getNome());
        txtEmail.setText(usuario.getEmail());
        txtTelefone.setText(usuario.getTelefone());

        if (!usuario.getFotoUrl().isEmpty()){
            Glide.with(foto_Usuario.getContext())
                    .load(usuario.getFotoUrl())
                    .into(foto_Usuario);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        DocumentReference docRef = firebaseFirestore.collection("usuario").document(mFirebaseAuth.getCurrentUser().getUid());


        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.exists()) {

                        preencheDados(document.toObject(Usuario.class));
                    }
                } else {
                    Log.d("Falha: ", "get failed with ", task.getException());
                }

            }
        });
    }
}
