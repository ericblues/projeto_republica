package repsys.teste.com.repsys;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import republica.Endereco;
import republica.Imovel;
import republica.Republica;
import republica.RestricoesRepublica;

public class VisualizarRepublica extends AppCompatActivity {


    private ImageView imageView;
    //ViewFLipper
    private ViewFlipper vf = null;
    private FloatingActionButton fab;

    private Button btnSobre;
    private Button btnRestricoes;
    private Button btnEndereco;

    private TextView nomeRepublica;
    private TextView descricaoRepublica;
    private TextView valorAluguel;
    private TextView contasInclusas;
    private TextView qtdeVagas;
    private TextView tipoImovel;
    private TextView internet;
    private TextView estacionamento;
    private TextView qtdeQuartos;
    private TextView qtdeBanheiros;
    private TextView qtdeSuites;

    private TextView cep;
    private TextView rua;
    private TextView numero;
    private TextView bairro;
    private TextView complemento;
    private TextView cidade;
    private TextView estado;

    private Republica republica;
    private Endereco endereco;
    private RestricoesRepublica restricoesRepublica;
    private Imovel imovel;

    private FirebaseAuth mFirebaseAuth;

    private FirebaseFirestore firebaseFirestore;
    private DocumentReference republicaReferencia;


    //progress dialog
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_visualizar_republica);

        mFirebaseAuth = FirebaseAuth.getInstance();

        republica = (Republica) getIntent().getSerializableExtra("republica");


        firebaseFirestore = FirebaseFirestore.getInstance();
        republicaReferencia = firebaseFirestore.collection("republica").document(republica.getIdRepublica());


        imageView = (ImageView) findViewById(R.id.imageViewVisualiarRepublica);
        vf = (ViewFlipper) findViewById(R.id.vfVizualizarRepublica);
        btnSobre = (Button) findViewById(R.id.btnSobre);
        btnRestricoes = (Button) findViewById(R.id.btnRestricoes);
        btnEndereco = (Button) findViewById(R.id.btnEndereco);


        if (!republica.getUrlFoto().isEmpty()){
            Glide.with(imageView.getContext())
                    .load(republica.getUrlFoto())
                    .into(imageView);
        }


        nomeRepublica = (TextView) findViewById(R.id.txtNomeRep);
        nomeRepublica.setText(republica.getNomeRep());

        descricaoRepublica = (TextView) findViewById(R.id.txtDescRep);

        valorAluguel = (TextView) findViewById(R.id.txtValorAluguel);
        contasInclusas = (TextView) findViewById(R.id.txtContasInclusas);
        qtdeVagas = (TextView) findViewById(R.id.txtQtdeVagas);
        tipoImovel = (TextView) findViewById(R.id.txtTipoImovel);
        internet = (TextView) findViewById(R.id.txtInternet);
        estacionamento = (TextView) findViewById(R.id.txtEstacionamento);
        qtdeQuartos = (TextView) findViewById(R.id.txtQtdeQuartos);
        qtdeSuites = (TextView) findViewById(R.id.txtQtdeSuite);
        cep = (TextView) findViewById(R.id.txtCEP);
        rua = (TextView) findViewById(R.id.txtRua);
        numero = (TextView) findViewById(R.id.txtNumero);
        bairro = (TextView) findViewById(R.id.txtBairro);
        complemento = (TextView) findViewById(R.id.txtComplemento);
        cidade = (TextView) findViewById(R.id.txtCidade);
        estado = (TextView) findViewById(R.id.txtEstado);

        btnSobre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vf.setDisplayedChild(0);
            }
        });

        btnRestricoes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vf.setDisplayedChild(1);
            }
        });

        btnEndereco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vf.setDisplayedChild(2);
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fabEnviarSolicitacao);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });


        //Recuperando o restantes dos dados da república que estão na subcoleção sobre
        //Inicializando o progress dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Carregando informações, aguarde...");
        progressDialog.show();

        carregaEndereco();


    }


    private void carregaEndereco(){
        republicaReferencia.collection("sobre").document("endereco")
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        endereco = documentSnapshot.toObject(Endereco.class);
                        carregaImovel();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("Erro: ", e.toString());
            }
        });
    }

    private void carregaImovel(){
        republicaReferencia.collection("sobre").document("imovel")
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        imovel = documentSnapshot.toObject(Imovel.class);
                        carregaRestricoes();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("Erro: ", e.toString());
            }
        });
    }

    private void carregaRestricoes(){
        republicaReferencia.collection("sobre").document("restricoes")
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        restricoesRepublica = documentSnapshot.toObject(RestricoesRepublica.class);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Magic here
                                preencheInformacoes();
                            }
                        }, 2000);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("Erro: ", e.toString());
            }
        });
    }

    private void showDialog(){
        new AlertDialog.Builder(VisualizarRepublica.this)
                .setTitle("Soliciar vaga")
                .setMessage("Tem certeza que deseja solicitar essa vaga?")
                .setPositiveButton("Sim",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                solicitarVaga(mFirebaseAuth.getCurrentUser().getUid() , republica.getIdRepublica());
                            }
                        })
                .setNegativeButton("Não", null)
                .show();
    }


    private void solicitarVaga(String idUsuario, String idRepublica){
        Log.i("Id Usuario: ", idUsuario);
        Log.i("Id Republica", idRepublica);
    }

    private void preencheInformacoes(){

        valorAluguel.setText("Valor do Aluguel: " + republica.getValorAluguel());
        if (republica.isContasInclusas())
            contasInclusas.setText("Contas inclusas: Sim");
        else
            contasInclusas.setText("Contas inclusas: Não");
         qtdeVagas.setText("Vagas: " + republica.getQtdeVagas());
        tipoImovel.setText("Tipo de imóvel: " + imovel.getTipoImovel());
         if (imovel.isInternet())
            internet.setText("Internet: Sim");
        else
            internet.setText("Internet: Não");
        if (imovel.isEstacionamento())
            estacionamento.setText("Estacionamento: Sim");
        else
           estacionamento.setText("Estacionamento: Não");
        qtdeQuartos.setText("Quartos: " + imovel.getQtdeQuartos());
        qtdeBanheiros = (TextView) findViewById(R.id.txtQtdeBanheiros);
        qtdeBanheiros.setText("Banheiros: " + imovel.getQtdeBanheiros());
        qtdeSuites.setText("Suítes: " + imovel.getN_suite());
        cep.setText("CEP: " + endereco.getCep());
        rua.setText("Rua: " + endereco.getRua());
        numero.setText("Número: " + endereco.getNumero());
        bairro.setText("Bairro: " + endereco.getBairro());
        complemento.setText("Complemento: " + endereco.getComplemento());
        cidade.setText("Cidade: " + endereco.getCidade());
        estado.setText("Estado: " + endereco.getEstado());

        progressDialog.dismiss();

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
