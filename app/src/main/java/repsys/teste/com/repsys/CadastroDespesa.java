package repsys.teste.com.repsys;

import android.app.DatePickerDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import despesas.Despesa;
import sharedpreferences.ShHelper;
import usuario.Usuario;

public class CadastroDespesa extends AppCompatActivity {



    private Spinner spnTipoDespesa;
    private EditText edtTituloDespesa;
    private EditText edtValorTotalDespesa;
    private Button btnCadastrarDespesa;
    private Button btnCancelar;
    private EditText edtDataVencimentoDespesa;
    private ImageButton btnDataVencimentoDespesa;
    private int mYear, mMonth, mDay, mHour, mMinute;


    private String tipoDespesa;
    private String tituloDespesa;
    private Double valorTotalDespesa;

    private FirebaseFirestore firebaseFirestore;
    private DocumentReference despesasReference;
    private ShHelper shHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_cadastro_despesa);

        spnTipoDespesa = (Spinner) findViewById(R.id.spnTipoDespesa);
        edtTituloDespesa = (EditText) findViewById(R.id.edtTituloDespesa);
        edtValorTotalDespesa = (EditText) findViewById(R.id.edtValorTotalDespesa);
        btnCadastrarDespesa = (Button) findViewById(R.id.btnCadastrarDespesa);
        btnCancelar = (Button) findViewById(R.id.btnCancelarDespesa);

        edtDataVencimentoDespesa = (EditText) findViewById(R.id.edtDataVencimentoDespesa);
        btnDataVencimentoDespesa = (ImageButton) findViewById(R.id.btnDataVencimentoDespesa);
        edtDataVencimentoDespesa.setFocusable(false);

        //instancia do banco
        firebaseFirestore = FirebaseFirestore.getInstance();
        shHelper = new ShHelper(getApplicationContext());


        btnCadastrarDespesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tipoDespesa = spnTipoDespesa.getSelectedItem().toString().trim();
                tituloDespesa = edtTituloDespesa.getText().toString();
                valorTotalDespesa = Double.valueOf(edtValorTotalDespesa.getText().toString());

                cadastrarDespesa(shHelper.resgatarDadosUsuario());
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnDataVencimentoDespesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(view.getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                edtDataVencimentoDespesa.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
    }


    private void cadastrarDespesa(final Usuario usuario){


        //referencia da despesa a ser cadastrada
        despesasReference = firebaseFirestore.collection("despesa").document(usuario.getIdRepublica()).collection("despesas").document();

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date dataVencimento = null;

        try {
            dataVencimento = formato.parse(edtDataVencimentoDespesa.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Despesa despesa = new Despesa(despesasReference.getId(), usuario.getIdRepublica(), tituloDespesa, valorTotalDespesa, valorTotalDespesa,
                0.0, tipoDespesa, dataVencimento, false, false);


        despesasReference.set(despesa).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getApplicationContext(), "Despesa cadastrada ...", Toast.LENGTH_SHORT).show();
                finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("Erro Deposito: ", "DocumentSnapshot deu pau: " + e);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
