package repsys.teste.com.repsys;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Arrays;

import sharedpreferences.ShHelper;
import usuario.Usuario;

public class TelaPrincipal extends AppCompatActivity {

    public static final String ANONYMOUS = "anonymous";
    public static final int RC_SIGN_IN = 1;

    private String mUsername;
    private String mEmail;
    private ProgressBar mProgressBar;
    private boolean mAdministrador;
    private boolean mMorador;

    //Firebase Instance Variables
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mUsersDataBaseReference;

    // Access a Cloud Firestore instance from your Activity

    private FirebaseFirestore firebaseFirestore;

    private Usuario dadosUsuario;

    private ShHelper shHelper;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        Fragment fragment = null;

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new PrimeiroAcessoFragment();
                    break;
                    //if (mAdministrador == true || mMorador == true){
                    //    findViewById(R.id.includePrimeiroAcesso).setVisibility(View.GONE);
                    //}
                case R.id.navigation_despesas:
                    fragment = new MenuDespesaFragment();
                    break;
                case R.id.navigation_profile:
                    fragment = new PerfilFragment();
                    break;
            }
            return loadFragment(fragment);
        }
    };


    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_tela_principal);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mUsername = ANONYMOUS;


        //Inicializando componentes do firebase
        firebaseFirestore = FirebaseFirestore.getInstance();

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mUsersDataBaseReference = mFirebaseDatabase.getReference().child("usuarios");

        // Initialize references to views
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        // Initialize progress bar
        mProgressBar.setVisibility(ProgressBar.INVISIBLE);

        //Inicializando shHelper
        shHelper = new ShHelper(getApplicationContext());



        //Aqui vai o código para verificar se o usuário está logado

        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();

                if (user != null){
                    //User is signed in
                    String nome = user.getDisplayName();
                    String email = user.getEmail();
                    onSignedInInitialize(nome, email);
                }else{
                    onSignedOutCleanup();
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setIsSmartLockEnabled(false)
                                    .setAvailableProviders(Arrays.asList(
                                            new AuthUI.IdpConfig.EmailBuilder().build(),

                                            new AuthUI.IdpConfig.GoogleBuilder().build()
                                    )).build(),
                            RC_SIGN_IN);
                }
            }
        };

        //loading the default fragment
        loadFragment(new PrimeiroAcessoFragment());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //logout
            AuthUI.getInstance().signOut(this);
            return true;
        }
        if (id == R.id.action_notificacoes){
            //Toast.makeText(getApplicationContext(), "Em produção", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), SolicitacoesRecebidas.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN){
            if (resultCode == RESULT_OK){
                Toast.makeText(this, "Signed in", Toast.LENGTH_SHORT).show();
                Log.i("SOBRE: ","Usuário " + mFirebaseAuth.getCurrentUser().getDisplayName() + " está logado");
            }else if (resultCode == RESULT_CANCELED){
                Toast.makeText(this, "Sign in canceled", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAuthStateListener != null){
            mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
        }
        //detachDatabaseReadListener();
        //mMessageAdapter.clear();
    }

    //Métodos para verificação de login
    private void onSignedInInitialize(String username, String email){
        mUsername = username;
        mEmail = email;

        verificaCadastroUsuario(mFirebaseAuth.getCurrentUser().getUid());


        //attachDatabaseReadListener();


    }

    private void onSignedOutCleanup(){
        mUsername = ANONYMOUS;
        //mMessageAdapter.clear();
        //detachDatabaseReadListener();
    }

    //Método de adicionar usuário
    private void addUser(){

        String uid = mFirebaseAuth.getCurrentUser().getUid();
        String nome = mFirebaseAuth.getCurrentUser().getDisplayName();
        String email = mFirebaseAuth.getCurrentUser().getEmail();
        String telefone = mFirebaseAuth.getCurrentUser().getPhoneNumber();
        Uri fotoUri = null;
        fotoUri = mFirebaseAuth.getCurrentUser().getPhotoUrl();
        String fotoUrl;

        try{
            fotoUrl = fotoUri.toString();
        }catch (Exception e){
            Log.i("INFO: ", "USUARIO NAO POSSUI FOTO DE PERFIL");
            fotoUrl = "";
        }

        final Usuario usuario = new Usuario(uid,"", nome, email, telefone, false, false, fotoUrl);

        firebaseFirestore.collection("usuario").document(uid)
                .set(usuario)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("Fire Store", "DocumentSnapshot successfully written!");
                        shHelper.salvarDadosUsuario(usuario);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("Erro: ", "DocumentSnapshot deu pau: " + e);
            }
        });

    }


    private void verificaCadastroUsuario(String uid){

        //DatabaseReference mConsultaUsuario = mFirebaseDatabase.getReference("usuarios");



        //Documentação do google diz o seguinte:
        //O exemplo a seguir mostra como recuperar o conteúdo de um único documento usando get():
        //Aqui estou tentando pegar o documento do uid do usuário. Se o documento não existir, quer dizer que ainda não foi criado
        //Se existir, o usuário já logou pelo menos uma vez


        DocumentReference docRef = firebaseFirestore.collection("usuario").document(uid);


        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.exists()) {
                        Log.d("Usuário já cadastrado: ", "DocumentSnapshot data: " + document.getData());
                        dadosUsuario = document.toObject(Usuario.class);

                        shHelper.salvarDadosUsuario(dadosUsuario);
                    } else {
                        Log.d("Informação: ", "Usuário não cadastrado");

                        //Aqui, se o documento não foi encontrado, é realizado o cadastro do usuário
                        addUser();
                    }
                } else {
                    Log.d("Falha: ", "get failed with ", task.getException());
                }

            }
        });

    }
    private void loadActivity(){
        DatabaseReference mUsersDataBaseReference = mFirebaseDatabase.getReference("usuarios").child(mFirebaseAuth.getCurrentUser().getUid());


        // Attach a listener to read the data at our posts reference

        mUsersDataBaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Usuario user = dataSnapshot.getValue(Usuario.class);
                mAdministrador = user.isAdministrador();
                mMorador = user.isMorador();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


}
