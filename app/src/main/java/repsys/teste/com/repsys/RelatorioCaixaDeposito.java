package repsys.teste.com.repsys;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.anychart.anychart.AnyChart;
import com.anychart.anychart.AnyChartView;
import com.anychart.anychart.Cartesian;
import com.anychart.anychart.CartesianSeriesColumn;
import com.anychart.anychart.DataEntry;
import com.anychart.anychart.EnumsAnchor;
import com.anychart.anychart.HoverMode;
import com.anychart.anychart.Position;
import com.anychart.anychart.TooltipPositionMode;
import com.anychart.anychart.ValueDataEntry;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import caixa.Caixa;
import caixa.TelaCaixa3;
import sharedpreferences.ShHelper;
import usuario.Usuario;

public class RelatorioCaixaDeposito extends AppCompatActivity {


    private ShHelper shHelper;
    private FirebaseFirestore firebaseFirestore;
    private CollectionReference depositosRef;

    private List<Caixa> caixaList;



    private HashMap<String, Double> listaMesValor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorio_caixa_deposito);

        caixaList = new ArrayList<>();


        listaMesValor = new HashMap<>();

        listaMesValor.put("Janeiro", 0.0);
        listaMesValor.put("Fevereiro", 0.0);
        listaMesValor.put("Março", 0.0);
        listaMesValor.put("Abril", 0.0);
        listaMesValor.put("Maio", 0.0);
        listaMesValor.put("Junho", 0.0);
        listaMesValor.put("Julho", 0.0);
        listaMesValor.put("Agosto", 0.0);
        listaMesValor.put("Setembro", 0.0);
        listaMesValor.put("Outubro", 0.0);
        listaMesValor.put("Novembro", 0.0);
        listaMesValor.put("Dezembro", 0.0);



        shHelper = new ShHelper(getApplicationContext());
        Usuario usuario = shHelper.resgatarDadosUsuario();

        firebaseFirestore = FirebaseFirestore.getInstance();


        depositosRef = firebaseFirestore.collection("caixa").document(usuario.getIdRepublica()).collection("depositos");

        Date dataDe = (Date) getIntent().getSerializableExtra("dataDe");
        Date dataAte = (Date) getIntent().getSerializableExtra("dataAte");

        relatorioDeposito(dataDe, dataAte);

    }




    public void relatorioDeposito(Date dataDe, Date dataAte){

        Query queryDataDeposito = depositosRef
                .whereGreaterThanOrEqualTo("data", dataDe)
                .whereLessThanOrEqualTo("data", dataAte)
                .orderBy("data", Query.Direction.ASCENDING);

        queryDataDeposito.get()
                .addOnCompleteListener(this, new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {


                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {

                                Caixa caixa = document.toObject(Caixa.class);
                                caixaList.add(caixa);
                            }

                            Log.i("Lista: ", caixaList.toString());

                            calcularDepositoMensal(caixaList);
                        } else {
                            Log.d("RETORNO: ", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    public void calcularDepositoMensal(List<Caixa> depositos){

        int i = 0;

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        for (i = 0; i < depositos.size(); i++){

            int mes = getMonth(depositos.get(i).getData());
            Double valor;

            switch (mes){
                case 0:
                    valor = listaMesValor.get("Janeiro");
                    valor += depositos.get(i).getValor();
                    listaMesValor.put("Janeiro", valor);
                    break;
                case 1:
                    valor = listaMesValor.get("Fevereiro");
                    valor += depositos.get(i).getValor();
                    listaMesValor.put("Fevereiro", valor);
                    break;
                case 2:
                    valor = listaMesValor.get("Março");
                    valor += depositos.get(i).getValor();
                    listaMesValor.put("Março", valor);
                    break;
                case 3:
                    valor = listaMesValor.get("Abril");
                    valor += depositos.get(i).getValor();
                    listaMesValor.put("Abril", valor);
                    break;
                case 4:
                    valor = listaMesValor.get("Maio");
                    valor += depositos.get(i).getValor();
                    listaMesValor.put("Maio", valor);
                    break;
                case 5:
                    valor = listaMesValor.get("Junho");
                    valor += depositos.get(i).getValor();
                    listaMesValor.put("Junho", valor);
                    break;
                case 6:
                    valor = listaMesValor.get("Julho");
                    valor += depositos.get(i).getValor();
                    listaMesValor.put("Julho", valor);
                    break;
                case 7:
                    valor = listaMesValor.get("Agosto");
                    valor += depositos.get(i).getValor();
                    listaMesValor.put("Agosto", valor);
                    break;
                case 8:
                    valor = listaMesValor.get("Setembro");
                    valor += depositos.get(i).getValor();
                    listaMesValor.put("Setembro", valor);
                    break;
                case 9:
                    valor = listaMesValor.get("Outubro");
                    valor += depositos.get(i).getValor();
                    listaMesValor.put("Outubro", valor);
                    break;
                case 10:
                    valor = listaMesValor.get("Novembro");
                    valor += depositos.get(i).getValor();
                    listaMesValor.put("Novembro", valor);
                    break;
                case 11:
                    valor = listaMesValor.get("Dezembro");
                    valor += depositos.get(i).getValor();
                    listaMesValor.put("Dezembro", valor);
                    break;


            }

        }

        List<DataEntry> data = new ArrayList<>();


        if (listaMesValor.get("Janeiro").doubleValue() > 0){
            data.add(new ValueDataEntry("Janeiro", listaMesValor.get("Janeiro").doubleValue()));
        }
        if (listaMesValor.get("Fevereiro").doubleValue() > 0){
            data.add(new ValueDataEntry("Fevereiro", listaMesValor.get("Fevereiro").doubleValue()));
        }
        if (listaMesValor.get("Março").doubleValue() > 0){
            data.add(new ValueDataEntry("Março", listaMesValor.get("Março").doubleValue()));
        }
        if (listaMesValor.get("Abril").doubleValue() > 0){
            data.add(new ValueDataEntry("Abril", listaMesValor.get("Abril").doubleValue()));
        }
        if (listaMesValor.get("Maio").doubleValue() > 0){
            data.add(new ValueDataEntry("Maio", listaMesValor.get("Maio").doubleValue()));
        }
        if (listaMesValor.get("Junho").doubleValue() > 0){
            data.add(new ValueDataEntry("Junho", listaMesValor.get("Junho").doubleValue()));
        }
        if (listaMesValor.get("Julho").doubleValue() > 0){
            data.add(new ValueDataEntry("Julho", listaMesValor.get("Julho").doubleValue()));
        }
        if (listaMesValor.get("Agosto").doubleValue() > 0){
            data.add(new ValueDataEntry("Agosto", listaMesValor.get("Agosto").doubleValue()));
        }
        if (listaMesValor.get("Setembro").doubleValue() > 0){
            data.add(new ValueDataEntry("Setembro", listaMesValor.get("Setembro").doubleValue()));
        }
        if (listaMesValor.get("Outubro").doubleValue() > 0){
            data.add(new ValueDataEntry("Outubro", listaMesValor.get("Outubro").doubleValue()));
        }
        if (listaMesValor.get("Novembro").doubleValue() > 0){
            data.add(new ValueDataEntry("Novembro", listaMesValor.get("Novembro").doubleValue()));
        }
        if (listaMesValor.get("Dezembro").doubleValue() > 0){
            data.add(new ValueDataEntry("Dezembro", listaMesValor.get("Dezembro").doubleValue()));
        }


        graficoBarra(data);
    }

    public static int getMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH);
    }

    public void graficoBarra(List<DataEntry> dados){


        Cartesian cartesian = AnyChart.column();

        //List<DataEntry> data = dados;
        /*data.add(new ValueDataEntry("Rouge", 80540));
        data.add(new ValueDataEntry("Foundation", 94190));
        data.add(new ValueDataEntry("Mascara", 102610));
        data.add(new ValueDataEntry("Lip gloss", 110430));
        data.add(new ValueDataEntry("Lipstick", 128000));
        data.add(new ValueDataEntry("Nail polish", 143760));
        data.add(new ValueDataEntry("Eyebrow pencil", 170670));
        data.add(new ValueDataEntry("Eyeliner", 213210));
        data.add(new ValueDataEntry("Eyeshadows", 249980));*/

        CartesianSeriesColumn column = cartesian.column(dados);

        column.getTooltip()
                .setTitleFormat("{%X}")
                .setPosition(Position.CENTER_BOTTOM)
                .setAnchor(EnumsAnchor.CENTER_BOTTOM)
                .setOffsetX(0d)
                .setOffsetY(5d)
                .setFormat("${%Value}{groupsSeparator: }");

        cartesian.setAnimation(true);
        cartesian.setTitle("Relatório por intervalo de datas");

        cartesian.getYScale().setMinimum(0d);

        cartesian.getYAxis().getLabels().setFormat("${%Value}{groupsSeparator: }");

        cartesian.getTooltip().setPositionMode(TooltipPositionMode.POINT);
        cartesian.getInteractivity().setHoverMode(HoverMode.BY_X);

        cartesian.getXAxis().setTitle("Mês");
        cartesian.getYAxis().setTitle("Valor");

        try{
            AnyChartView anyChartView;

            anyChartView = (AnyChartView) findViewById(R.id.chartCaixaDeposito);

            anyChartView.setChart(cartesian);
        }catch (Exception e){
            Log.i("Erro chart: ", e.toString());
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
