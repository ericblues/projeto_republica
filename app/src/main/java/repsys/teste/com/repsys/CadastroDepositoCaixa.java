package repsys.teste.com.repsys;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import caixa.Caixa;
import republica.Republica;
import usuario.Usuario;

public class CadastroDepositoCaixa extends AppCompatActivity {

    private Spinner spnMorador;
    private EditText edtValor;
    private EditText edtData;
    private Button btnCadastrar;
    private Button btnCancelar;
    private SimpleDateFormat formato;


    private String morador;
    private Date data;
    private Double valorDeposito;

    private FirebaseFirestore firebaseFirestore;

    private DocumentReference depositosReference;

    private FirebaseAuth mFirebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_cadastro_deposito_caixa);

        //criando referencia para os componentes da tela
        spnMorador = (Spinner) findViewById(R.id.spnCaixaMorador);
        edtValor = (EditText) findViewById(R.id.edtValorContribuicao);
        edtData = (EditText) findViewById(R.id.edtDataContribuicao);
        btnCadastrar = (Button) findViewById(R.id.btnDepositoCadastrar);
        btnCancelar = (Button) findViewById(R.id.btnDepositoCancelar);


        //instancia do usuario logado
        mFirebaseAuth = FirebaseAuth.getInstance();


        //instancia do banco
        firebaseFirestore = FirebaseFirestore.getInstance();

        //formato da data
        formato = new SimpleDateFormat("dd/MM/yyyy");


        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                morador = spnMorador.getSelectedItem().toString().trim();
                data = null;
                valorDeposito = Double.parseDouble(edtValor.getText().toString());
                try {
                     data = formato.parse(edtData.getText().toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                getDadosUsuario();

            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getDadosUsuario(){

        DocumentReference usuarioRef = firebaseFirestore.collection("usuario").document(mFirebaseAuth.getCurrentUser().getUid());


        usuarioRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.exists()) {

                        cadastrarDeposito(document.toObject(Usuario.class));

                    } else {
                        Log.d("Informação: ", "Problema ao consultar dados para a user: " + mFirebaseAuth.getCurrentUser().getUid());


                    }
                } else {
                    Log.d("Falha: ", "get failed with ", task.getException());
                }

            }
        });
    }

    private void cadastrarDeposito(final Usuario usuario){

        Caixa deposito = new Caixa(usuario.getIdRepublica(), "", morador, valorDeposito, data);

        //referencia do depósito a ser realizado
        depositosReference = firebaseFirestore.collection("caixa").document(usuario.getIdRepublica()).collection("depositos").document();


        depositosReference.set(deposito).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("Deposito Realizado", "DocumentSnapshot successfully written!");

                getDadosRepublica(usuario.getIdRepublica());
                //finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("Erro Deposito: ", "DocumentSnapshot deu pau: " + e);
            }
        });
    }

    private void getDadosRepublica(final String idRepublica){
        DocumentReference republicaRef = firebaseFirestore.collection("republica").document(idRepublica);


        republicaRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.exists()) {

                        atualizarCaixa(document.toObject(Republica.class));

                    } else {
                        Log.d("Informação: ", "Problema ao consultar dados para a rep: " + idRepublica);


                    }
                } else {
                    Log.d("Falha: ", "get failed with ", task.getException());
                }

            }
        });
    }

    private void atualizarCaixa(Republica republica){

        Double novoValor = republica.getValorCaixa() + valorDeposito;

        firebaseFirestore.collection("republica").document(republica.getIdRepublica()).update("valorCaixa", novoValor).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("Atualizado: ", "DocumentSnapshot successfully updated!");

                Toast.makeText(getApplicationContext(), "Depósito realizado ...", Toast.LENGTH_SHORT).show();
                finish();
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("Não atualizado:", "Error updating document", e);
                    }
                });
    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
