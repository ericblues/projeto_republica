package repsys.teste.com.repsys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class VisualizarRecibo extends AppCompatActivity {


    private ImageView imgRecibo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_visualizar_recibo);

        imgRecibo = (ImageView) findViewById(R.id.imageViewRecibo);


        String urlRecibo = (String) getIntent().getSerializableExtra("urlRecibo");

        if (!urlRecibo.isEmpty()){
            Glide.with(imgRecibo.getContext())
                    .load(urlRecibo)
                    .into(imgRecibo);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
