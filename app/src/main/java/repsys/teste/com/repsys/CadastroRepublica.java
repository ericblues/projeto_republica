package repsys.teste.com.repsys;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import republica.Endereco;
import republica.Imovel;
import republica.Republica;
import republica.RestricoesRepublica;

public class CadastroRepublica extends AppCompatActivity {



    // Access a Cloud Firestore instance from your Activity

    FirebaseFirestore firebaseFirestore;


    private static final int RC_PHOTO_PICKER =  2;

    private String idRepublica = "";

    private int status_code;

    //Firebase Instance Variables
    private FirebaseDatabase mFirebaseDatabase;


    private DatabaseReference mRepublicaDataBaseReference;
    private DatabaseReference mImovelDataBaseReference;
    private DatabaseReference mRestricoesDataBaseReference;
    private DatabaseReference mEnderecoDataBaseReference;


    private FirebaseAuth mFirebaseAuth;
    private FirebaseStorage mFirebaseStorage;
    private StorageReference mRepublicaPhotoStorageReference;


    //Objeto de requisição padrão
    private JsonObjectRequest requisicao = null;

    //ViewFLipper
    private ViewFlipper vf = null;

    //Buttons
    private Button btnAvancar;
    private Button btnRetroceder;
    private Button btnAdicionarFoto;
    private Button btnAdicionarMaps;

    //EDIT TEXTS

    private EditText edtNomeRep;
    private EditText edtValorAluguel;
    private EditText edtQtdeVagas;
    private EditText edtQtdeQuartos;
    private EditText edtQtdeBanheiros;


    private EditText edtCEP;
    private EditText edtRUA;
    private EditText edtNUMERO;
    private EditText edtBAIRRO;
    private EditText edtCOMPLEMENTO;
    private EditText edtCIDADE;

    //Spinner
    private Spinner spnContasInclusas;
    private Spinner spnTipoRepublica;
    private Spinner spnTipoImovel;
    private Spinner spnInternet;
    private Spinner spnEstacionamento;
    private Spinner spnSuite;
    private Spinner spnQuartosCompartilhados;
    private Spinner spnFumantes;
    private Spinner spnAnimais;
    private Spinner spnCriancas;
    private Spinner spnESTADO;

    //progress dialog
    private ProgressDialog progressDialog;


    private DocumentReference republicaReferencia;

    private Map<String, Object> indicesMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_cadastro_republica);

        // Initialize Firebase components


        firebaseFirestore = FirebaseFirestore.getInstance();


        //AQui é gerado o id do documento da república
        //cada documento possui uma coleção com seus devidos campos
        republicaReferencia = firebaseFirestore.collection("republica").document();
        idRepublica = republicaReferencia.getId();
        indicesMap = new HashMap<>(); //Hash Map criado para armazenar os indíces da busca



        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseStorage = FirebaseStorage.getInstance();


        //Refêrenciando as EditTexts
        edtNomeRep = (EditText) findViewById(R.id.edtNomeRepublica);
        edtValorAluguel = (EditText) findViewById(R.id.edtValorAluguel);
        edtQtdeVagas = (EditText) findViewById(R.id.edtQtdeVagas);
        edtQtdeQuartos = (EditText) findViewById(R.id.edtQtdeQuartos);
        edtQtdeBanheiros = (EditText) findViewById(R.id.edtQtdeBanheiros);

        edtCEP = (EditText) findViewById(R.id.edtCEP);
        edtRUA = (EditText) findViewById(R.id.edtRUA);
        edtNUMERO = (EditText) findViewById(R.id.edtNUMERO);
        edtBAIRRO = (EditText) findViewById(R.id.edtBAIRRO);
        edtCOMPLEMENTO = (EditText) findViewById(R.id.edtCOMPLEMENTO);
        edtCIDADE = (EditText) findViewById(R.id.edtCIDADE);


        spnContasInclusas = (Spinner) findViewById(R.id.spnContasInclusas);
        spnTipoRepublica = (Spinner) findViewById(R.id.spnTipoRepublica);
        spnTipoImovel = (Spinner) findViewById(R.id.spnTipoImovel);
        spnInternet = (Spinner) findViewById(R.id.spnInternet);
        spnEstacionamento = (Spinner) findViewById(R.id.spnEstacionamento);
        spnSuite = (Spinner) findViewById(R.id.spnSuite);
        spnQuartosCompartilhados = (Spinner) findViewById(R.id.spnQuartosCompartilhados);
        spnFumantes = (Spinner) findViewById(R.id.spnFumantes);
        spnAnimais = (Spinner) findViewById(R.id.spnAnimais);
        spnCriancas = (Spinner) findViewById(R.id.spnCriancas);
        spnESTADO = (Spinner) findViewById(R.id.spnESTADO);


        vf = (ViewFlipper) findViewById(R.id.vfCadastroRepublica);

        btnAvancar = (Button) findViewById(R.id.btnAvancar);
        btnRetroceder = (Button) findViewById(R.id.btnRetroceder);

        btnAdicionarFoto = (Button) findViewById(R.id.btnAdicionarFoto);
        btnAdicionarMaps = (Button) findViewById(R.id.btnAdicionarMaps);

        //Inicializando o progress dialog
        progressDialog = new ProgressDialog(this);

        //Código que adiciona máscara ao campo CEP
        edtCEP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String working = s.toString();

                if (s.length() == 5 && before == 0){
                    working += "-";
                    edtCEP.setText(working);
                    edtCEP.setSelection(working.length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 9){

                    progressDialog.setMessage("Procurando dados para o CEP informado, aguarde...");
                    progressDialog.show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Magic here
                            consultaCEP();
                        }
                    }, 2000);

                }
            }
        });

        btnAvancar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (vf.getDisplayedChild()){
                    case 0:
                        btnRetroceder.setVisibility(View.VISIBLE);
                        vf.setDisplayedChild(1);
                        break;
                    case 1:
                        vf.setDisplayedChild(2);
                        preencherSpnQuartos();
                        break;
                    case 2:
                        vf.setDisplayedChild(3);
                        btnAvancar.setText("Cadastrar");
                        break;
                    case 3:
                        //vf.setDisplayedChild(4);
                        btnAvancar.setText("Finalizar");
                        btnRetroceder.setVisibility(View.GONE);
                        progressDialog.setMessage("Cadastrando dados, aguarde...");
                        progressDialog.show();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Magic here
                                cadastraRepublica();
                            }
                        }, 3000);
                        break;
                    case 4:
                        finish();
                        break;
                    default:
                        break;
                }
            }
        });

        btnRetroceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (vf.getDisplayedChild()){
                    case 0:
                        break;
                    case 1:
                        btnRetroceder.setVisibility(View.GONE);
                        vf.setDisplayedChild(0);
                        break;
                    case 2:
                        vf.setDisplayedChild(1);
                        break;
                    case 3:
                        vf.setDisplayedChild(2);
                        btnAvancar.setText("Avançar");
                        break;
                    case 4:
                        btnRetroceder.setVisibility(View.GONE);
                        break;
                    default:
                        break;
                }
            }
        });

        btnAdicionarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Ação: Selecionar Foto", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/jpeg");
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(Intent.createChooser(intent, "Complete action using"), RC_PHOTO_PICKER);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        mRepublicaPhotoStorageReference = mFirebaseStorage.getReference().child("republica_photos/"+idRepublica);

        if (requestCode == RC_PHOTO_PICKER && resultCode == RESULT_OK){
            Uri selectedImageUri = data.getData();

            // Get a reference to store file at chat_photos/<FILENAME>
            StorageReference photoRef = mRepublicaPhotoStorageReference.child(selectedImageUri.getLastPathSegment());

            progressDialog.setMessage("Fazendo upload da foto, aguarde...");
            progressDialog.show();

            // Upload file to Firebase Storage
            photoRef.putFile(selectedImageUri).addOnSuccessListener(this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // When the image has successfully uploaded, we get its download URL

                    progressDialog.dismiss();

                    Uri downloadUrl = taskSnapshot.getDownloadUrl();

                    Toast.makeText(getApplicationContext(), "Foto adicionada com sucesso", Toast.LENGTH_SHORT).show();
                    try{
                        firebaseFirestore.collection("republica").document(idRepublica).update("urlFoto", downloadUrl.toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d("Foto info: ", "DocumentSnapshot successfully updated!");
                            }
                        })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.w("Foto info:", "Error updating document", e);
                                    }
                                });
                        //mFirebaseDatabase.getReference("republicas").child(idRepublica + "/urlFoto").setValue(downloadUrl.toString());
                    }catch (Exception e){
                        Log.i("ERRO AO SUBIR A FOTO: ", e.toString());
                    }
                }
            });
        }
    }

    private void preencheCampos(JSONObject dados) throws JSONException{

        edtRUA.setText(dados.getString("logradouro"));
        edtBAIRRO.setText(dados.getString("bairro"));
        edtCOMPLEMENTO.setText(dados.getString("complemento"));
        edtCIDADE.setText(dados.getString("localidade"));


        ArrayAdapter<String> adaptadorEstado = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.valoresEstados));
        adaptadorEstado.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnESTADO.setAdapter(adaptadorEstado);

        spnESTADO.setSelection(adaptadorEstado.getPosition(dados.getString("uf")));
    }

    private void consultaCEP(){
        String URL = "https://viacep.com.br/ws/"+edtCEP.getText().toString()+"/json/";

        requisicao = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.has("erro")){
                        Log.i("Erro: ", response.toString());
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "CEP inválido", Toast.LENGTH_SHORT).show();
                    }else{
                        preencheCampos(response);
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Dados Econtrados para o CEP", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Erro: ", error.toString());
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(requisicao);
    }

    private void cadastraRepublica(){

        String nomeRep = edtNomeRep.getText().toString().trim();
        double valorAluguel = Double.parseDouble(edtValorAluguel.getText().toString());
        boolean contasInclusas = false;
        int qtdeVagas = Integer.parseInt(edtQtdeVagas.getText().toString());
        String tipoRepublica = spnTipoRepublica.getSelectedItem().toString();

        if (spnContasInclusas.getSelectedItem().toString().equals("Sim"))
            contasInclusas = true;

        //mRepublicaDataBaseReference = mFirebaseDatabase.getReference("republica").child(mFirebaseAuth.getCurrentUser().getUid());

        //mudança feita aqui
        //agora a republica é independente do id do administrador,

       // mRepublicaDataBaseReference = mFirebaseDatabase.getReference().child("republicas");

        //idRepublica = mRepublicaDataBaseReference.push().getKey();


        //mRepublicaDataBaseReference.child(idRepublica).setValue(republica);



        //São feitos os cadastro das seguintes sub coleções antes do cadastro dos dados básicos da república
        //para assim gerar os índices que serão usados na busca

        cadastrarImovel();
        cadastrarRestricoes();
        cadastrarEndereco();


        Republica republica = new Republica(republicaReferencia.getId(), mFirebaseAuth.getCurrentUser().getUid(), nomeRep, valorAluguel, contasInclusas,
                qtdeVagas, tipoRepublica, "", 0.0, indicesMap);


        republicaReferencia.set(republica).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("Republica Cadastrada", "DocumentSnapshot successfully written!");
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("Erro: ", "DocumentSnapshot deu pau: " + e);
                    }
                });


        //Atualizando status de administrador do usuário
        //try {
        //    mFirebaseDatabase.getReference("usuarios").child(mFirebaseAuth.getUid() + "/administrador").setValue(true);
        //}catch (Exception e) {
        //    Log.i("Erro: ", e.toString());
        //}


        firebaseFirestore.collection("usuario").document(mFirebaseAuth.getCurrentUser().getUid()).update("administrador", true).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("Atualizado: ", "DocumentSnapshot successfully updated!");
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("Não atualizado:", "Error updating document", e);
                    }
                });

        firebaseFirestore.collection("usuario").document(mFirebaseAuth.getCurrentUser().getUid()).update("idRepublica", republica.getIdRepublica()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("Atualizado: ", "DocumentSnapshot successfully updated!");
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("Não atualizado:", "Error updating document", e);
                    }
                });
        progressDialog.dismiss();

        Toast.makeText(getApplicationContext(), "Cadastro realizado com sucesso!", Toast.LENGTH_SHORT).show();
        //finish();
        vf.setDisplayedChild(4);
    }


    private void cadastrarImovel(){

        String tipoImovel = spnTipoImovel.getSelectedItem().toString();
        boolean internet = false;
        boolean estacionamento = false;
        int qtdeQuartos = Integer.parseInt(edtQtdeQuartos.getText().toString());
        int qtdeBanheiros = Integer.parseInt(edtQtdeBanheiros.getText().toString());
        int n_suite = Integer.parseInt(spnSuite.getSelectedItem().toString());
        int n_quartosCompartilhados = Integer.parseInt(spnQuartosCompartilhados.getSelectedItem().toString());

        if (spnInternet.getSelectedItem().toString().equals("Sim"))
            internet = true;
        if (spnEstacionamento.getSelectedItem().toString().equals("Sim"))
            estacionamento = true;


        //mImovelDataBaseReference = mFirebaseDatabase.getReference("imovel").child(idRepublica);

        //String idImovel = mImovelDataBaseReference.push().getKey();

        //Aqui são inseridos no hash map os indices de busca para os dados do imóvel
        indicesMap.put("internet", internet);
        indicesMap.put("estacionamento", estacionamento);

        Imovel imovel = new Imovel(tipoImovel, internet, estacionamento, qtdeQuartos, qtdeBanheiros, n_suite, n_quartosCompartilhados);

        republicaReferencia.collection("sobre").document("imovel")
                .set(imovel)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("Imovel Cadastrado", "DocumentSnapshot successfully written!");
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("Erro: ", "DocumentSnapshot deu pau: " + e);
            }
        });

    }

    private void cadastrarRestricoes(){
        boolean fumantes = false;
        boolean animais = false;
        boolean visita = false;

        if (spnFumantes.getSelectedItem().toString().equals("Sim"))
            fumantes = true;
        if (spnAnimais.getSelectedItem().toString().equals("Sim"))
            animais = true;
        if (spnCriancas.getSelectedItem().toString().equals("Sim"))
            visita = true;

        //mRestricoesDataBaseReference = mFirebaseDatabase.getReference("restricoes").child(idRepublica);

       // String idRestricoes = mRestricoesDataBaseReference.push().getKey();

        //Aqui são inseridos no hash map os indices de busca para restrições
        indicesMap.put("fumantes", fumantes);
        indicesMap.put("animais", animais);
        indicesMap.put("visita", visita);

        RestricoesRepublica restricoesRepublica = new RestricoesRepublica(fumantes, animais, visita);

        republicaReferencia.collection("sobre").document("restricoes")
                .set(restricoesRepublica)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("Restricoes Cadastrado", "DocumentSnapshot successfully written!");
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("Erro: ", "DocumentSnapshot deu pau: " + e);
            }
        });
    }

    private void cadastrarEndereco(){

        //Feito alteração
        //tentando criar o nó de endereço dentro da república nova adicionada
        //sem id único, apenas com o nome endereço

        String cep = edtCEP.getText().toString().trim();
        String rua = edtRUA.getText().toString().trim();
        String numero = edtNUMERO.getText().toString().trim();
        String bairro = edtBAIRRO.getText().toString().trim();
        String complemento = edtCOMPLEMENTO.getText().toString().trim();
        String cidade = edtCIDADE.getText().toString().trim();
        String estado = spnESTADO.getSelectedItem().toString();

        //mEnderecoDataBaseReference = mFirebaseDatabase.getReference("republicas/" + idRepublica).child("endereco");

        //String idEndereco = mEnderecoDataBaseReference.push().getKey();

        //Aqui são inseridos no hash map os indices de busca para endereço
        indicesMap.put("cidade", cidade);
        indicesMap.put("estado", estado);

        Endereco endereco = new Endereco(cep, rua, numero, bairro, complemento, cidade, estado, "", "");

        //mEnderecoDataBaseReference.setValue(endereco);

        republicaReferencia.collection("sobre").document("endereco")
                .set(endereco)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("Endereco Cadastrado", "DocumentSnapshot successfully written!");
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("Erro: ", "DocumentSnapshot deu pau: " + e);
            }
        });
    }

    private void preencherSpnQuartos(){
        List<String> quantidade = new ArrayList<String>();

        for (int i = 0; i <= Integer.parseInt(edtQtdeQuartos.getText().toString()); i++){
            quantidade.add(String.valueOf(i));
        }

        //Cria um ArrayAdapter usando um padrão de layout da classe R do android, passando o ArrayList nomes
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, quantidade);
        ArrayAdapter<String> spinnerArrayAdapter = arrayAdapter;
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spnSuite.setAdapter(spinnerArrayAdapter);
        spnQuartosCompartilhados.setAdapter(spinnerArrayAdapter);
    }
}
