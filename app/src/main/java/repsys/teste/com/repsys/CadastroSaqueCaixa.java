package repsys.teste.com.repsys;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import caixa.Saque;
import republica.Republica;
import sharedpreferences.ShHelper;
import usuario.Usuario;

public class CadastroSaqueCaixa extends AppCompatActivity {


    private Spinner spnMotivoSaque;
    private EditText edtDescricao;
    private EditText edtValorSaque;
    private EditText edtDataSaque;
    private Button btnCadastrarSaque;
    private Button btnCancelarSaque;

    private String motivo;
    private String descricao;
    private Double valorSaque;
    private Date dataSaque;

    private SimpleDateFormat formato;

    private ShHelper shHelper;


    private FirebaseFirestore firebaseFirestore;

    private DocumentReference saquesReference;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_cadastro_saque_caixa);



        //instancia do banco
        firebaseFirestore = FirebaseFirestore.getInstance();


        //Inicializando shHelper para recuperar os dados do usuário
        shHelper = new ShHelper(getApplicationContext());

        //Objeto usuario para pegar os dados da republica que ele mora
        Usuario usuario = shHelper.resgatarDadosUsuario();



        spnMotivoSaque = (Spinner) findViewById(R.id.spnCaixaMotivoSaque);
        edtDescricao = (EditText) findViewById(R.id.edtDescSaque);
        edtValorSaque = (EditText) findViewById(R.id.edtValorSaque);
        edtDataSaque = (EditText) findViewById(R.id.edtDataSaque);
        btnCadastrarSaque = (Button) findViewById(R.id.btnSaqueCadastrar);
        btnCancelarSaque = (Button) findViewById(R.id.btnSaqueCancelar);

        btnCadastrarSaque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                motivo = spnMotivoSaque.getSelectedItem().toString().trim();
                descricao = edtDescricao.getText().toString();
                valorSaque = Double.valueOf(edtValorSaque.getText().toString());
                String edtData = "01/06/2018";
                dataSaque = Calendar.getInstance().getTime();
                /*try {
                    dataSaque = formato.parse(edtData);
                } catch (ParseException e) {
                    e.printStackTrace();
                }*/

                resgatarValorCaixa(shHelper.resgatarDadosUsuario());
            }
        });

        btnCancelarSaque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void resgatarValorCaixa(final Usuario usuario){


        DocumentReference republicaRef = firebaseFirestore.collection("republica").document(usuario.getIdRepublica());


        republicaRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.exists()) {

                        cadastrarSaque(document.toObject(Republica.class));

                    } else {
                        Log.d("Informação: ", "Problema ao consultar dados para a rep: " + usuario.getIdRepublica());
                    }
                } else {
                    Log.d("Falha: ", "get failed with ", task.getException());
                }
            }
        });
    }

    private void cadastrarSaque(final Republica republica){

        saquesReference = firebaseFirestore.collection("caixa").
                document(republica.getIdRepublica()).collection("saques").document();

        final Double novoValorCaixa = republica.getValorCaixa() - valorSaque;


        Saque saque = new Saque(saquesReference.getId(), republica.getIdRepublica(),
                motivo, descricao, valorSaque, republica.getValorCaixa(), novoValorCaixa, dataSaque);

        saquesReference.set(saque).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("Saque Realizado", "DocumentSnapshot successfully written!");


                atualizarCaixa(republica, novoValorCaixa);


                //finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("Erro Saque: ", "DocumentSnapshot deu pau: " + e);
            }
        });

    }

    private void atualizarCaixa(Republica republica, Double novoValorCaixa){


        firebaseFirestore.collection("republica").document(republica.getIdRepublica()).update("valorCaixa", novoValorCaixa).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("Atualizado: ", "DocumentSnapshot successfully updated!");

                Toast.makeText(getApplicationContext(), "Saque realizado ...", Toast.LENGTH_SHORT).show();
                finish();
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("Não atualizado:", "Error updating document", e);
                    }
                });
    }

}
