package repsys.teste.com.repsys;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.anychart.anychart.AnyChart;
import com.anychart.anychart.AnyChartView;
import com.anychart.anychart.DataEntry;
import com.anychart.anychart.Pie;
import com.anychart.anychart.ValueDataEntry;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import caixa.Caixa;
import caixa.Saque;
import sharedpreferences.ShHelper;
import usuario.Usuario;

public class RelatorioCaixaSaque extends AppCompatActivity {


    private AnyChartView anyChartView;

    private ShHelper shHelper;
    private FirebaseFirestore firebaseFirestore;
    private CollectionReference saquesRef;

    private List<Saque> saqueList;

    private List<String> listaTipoGasto;

    private HashMap<String, Double> listaTipoGastoValor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorio_caixa_saque);

        shHelper = new ShHelper(getApplicationContext());
        Usuario usuario = shHelper.resgatarDadosUsuario();

        firebaseFirestore = FirebaseFirestore.getInstance();

        saqueList = new ArrayList<>();
        listaTipoGasto = new ArrayList<>();
        listaTipoGastoValor = new HashMap<>();

        saquesRef = firebaseFirestore.collection("caixa").document(usuario.getIdRepublica()).collection("saques");

        Date dataDe = (Date) getIntent().getSerializableExtra("dataDe");
        Date dataAte = (Date) getIntent().getSerializableExtra("dataAte");

        relatorioSaque(dataDe, dataAte);

    }

    public void relatorioSaque(Date dataDe, Date dataAte){
        Query queryDataSaques = saquesRef
                .whereGreaterThanOrEqualTo("dataSaque", dataDe)
                .whereLessThanOrEqualTo("dataSaque", dataAte)
                .orderBy("dataSaque", Query.Direction.ASCENDING);

        queryDataSaques.get()
                .addOnCompleteListener(this, new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {

                                Saque saque = document.toObject(Saque.class);
                                saqueList.add(saque);
                            }

                            Log.i("Lista: ", saqueList.toString());

                            calcularGastosPorTipo(saqueList);

                        } else {
                            Log.d("RETORNO: ", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    public void calcularGastosPorTipo(List<Saque> saques){

        int i = 0;

        for (i = 0; i < saques.size(); i++){

            if (!listaTipoGasto.contains(saques.get(i).getMotivo())){
                listaTipoGasto.add(saques.get(i).getMotivo());
            }

        }

        for (i = 0; i < listaTipoGasto.size(); i++){
            listaTipoGastoValor.put(listaTipoGasto.get(i), 0.0);
        }


        for (i = 0; i < saques.size(); i++){

            String motivo = saques.get(i).getMotivo();

            Double valorSaqueSomatorio = listaTipoGastoValor.get(motivo).doubleValue();

            valorSaqueSomatorio += saques.get(i).getValorSacado();

            listaTipoGastoValor.put(motivo, valorSaqueSomatorio);

        }

        List<DataEntry> dados = new ArrayList<>();

        for (i = 0; i < listaTipoGasto.size(); i++){

            String motivo = listaTipoGasto.get(i);
            Double valorSaque = listaTipoGastoValor.get(listaTipoGasto.get(i));

            dados.add(new ValueDataEntry(motivo, valorSaque));

            Log.i("Motivo: ", motivo);
            Log.i("Saque: ", String.valueOf(valorSaque));
        }

        graficoPizza(dados);
    }

    public void graficoPizza(List<DataEntry> dados){
        Pie pie = AnyChart.pie();

        List<DataEntry> data = dados;

        pie.setData(data);


        anyChartView = (AnyChartView) findViewById(R.id.chartCaixaSaque);

        anyChartView.setChart(pie);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
