package repsys.teste.com.repsys;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.anychart.anychart.AnyChart;
import com.anychart.anychart.AnyChartView;
import com.anychart.anychart.DataEntry;
import com.anychart.anychart.Pie;
import com.anychart.anychart.ValueDataEntry;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import despesas.AtribuicaoDespesa;
import sharedpreferences.ShHelper;
import usuario.Usuario;

public class RelatorioPagoPendentePessoal extends AppCompatActivity {


    private AnyChartView anyChartView;

    private ShHelper shHelper;
    private FirebaseFirestore firebaseFirestore;
    private CollectionReference minhasDespesasRef;

    private List<AtribuicaoDespesa> minhasDespesasList;

    private HashMap<String, Double> listaDespesaValor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorio_pago_pendente_pessoal);

        shHelper = new ShHelper(getApplicationContext());
        Usuario usuario = shHelper.resgatarDadosUsuario();

        firebaseFirestore = FirebaseFirestore.getInstance();

        minhasDespesasList = new ArrayList<>();

        listaDespesaValor = new HashMap<>();

        listaDespesaValor.put("Pago", 0.0);
        listaDespesaValor.put("Pendente", 0.0);

        minhasDespesasRef = firebaseFirestore.collection("despesa").document(usuario.getIdRepublica())
                .collection("atribuicoes").document(usuario.getUid()).collection("minhasAtribuicoes");

        Date dataDe = (Date) getIntent().getSerializableExtra("dataDe");
        Date dataAte = (Date) getIntent().getSerializableExtra("dataAte");

        relatorioMinhasDespesasPagasPendentes(dataDe, dataAte);
    }

    private void relatorioMinhasDespesasPagasPendentes(Date dataDe, Date dataAte){
        Query queryDataDespesas = minhasDespesasRef
                .whereGreaterThanOrEqualTo("dataVencimento", dataDe)
                .whereLessThanOrEqualTo("dataVencimento", dataAte)
                .orderBy("dataVencimento", Query.Direction.ASCENDING);


        queryDataDespesas.get()
                .addOnCompleteListener(this, new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {

                                AtribuicaoDespesa minhaDespesa = document.toObject(AtribuicaoDespesa.class);
                                minhasDespesasList.add(minhaDespesa);
                            }

                            Log.i("Lista: ", minhasDespesasList.toString());

                            calcularGastosPorTipo(minhasDespesasList);

                        } else {
                            Log.d("RETORNO: ", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    public void calcularGastosPorTipo(List<AtribuicaoDespesa> minhasDespesas){

        int i = 0;

        for (i = 0; i < minhasDespesas.size(); i++){


            if (minhasDespesas.get(i).isSituacaoPagamento()){
                Double valorDespesaPagaSomatorio = listaDespesaValor.get("Pago").doubleValue();

                valorDespesaPagaSomatorio += minhasDespesas.get(i).getValorPago();

                listaDespesaValor.put("Pago", valorDespesaPagaSomatorio);
            }else{
                Double valorDespesaPendenteSomatorio = listaDespesaValor.get("Pendente").doubleValue();

                valorDespesaPendenteSomatorio += minhasDespesas.get(i).getValorPagar();

                listaDespesaValor.put("Pendente", valorDespesaPendenteSomatorio);
            }


        }

        List<DataEntry> dados = new ArrayList<>();



            String situacaoPago = "Pago";

            Double despesaPaga = listaDespesaValor.get("Pago");

            String situacaoPendente = "Pendente";

            Double despesaPendente = listaDespesaValor.get("Pendente");

            dados.add(new ValueDataEntry(situacaoPago, despesaPaga));
            dados.add(new ValueDataEntry(situacaoPendente, despesaPendente));


        graficoPizza(dados);
    }

    public void graficoPizza(List<DataEntry> dados){
        Pie pie = AnyChart.pie();

        List<DataEntry> data = dados;

        pie.setData(data);


        anyChartView = (AnyChartView) findViewById(R.id.chartDespesaPagoPendentePessoal);

        anyChartView.setChart(pie);
    }
}
