package repsys.teste.com.repsys;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import republica.CustomItemClickListener;
import republica.Endereco;
import republica.Republica;

public class RepublicaCardAdapter extends RecyclerView.Adapter<RepublicaCardAdapter.RepublicaViewHolder>{


    //this context we will use to inflate the layout
    private Context context;

    private List<Republica> republicaList;

    //private List<Endereco> enderecoList;


    CustomItemClickListener listener;

    public RepublicaCardAdapter(Context context, List<Republica> republicaList/*,List<Endereco> enderecoList*/, CustomItemClickListener listener){
        this.context = context;
        this.republicaList = republicaList;
        //this.enderecoList = enderecoList;
        this.listener = listener;
    }

    @Override
    public RepublicaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.layout_republica_card, null);

        final RepublicaViewHolder mViewHolder = new RepublicaViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mViewHolder.getPosition());
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(RepublicaViewHolder holder, int position) {


        //getting the republica of the specified position
        Republica republica = republicaList.get(position);

        //getting the endereco of the specified position
        //Endereco endereco = enderecoList.get(position);

        //binding the data with the viewholder views
        holder.textViewTitle.setText(republica.getNomeRep());
        holder.textViewPrice.setText("R$: " + String.valueOf(republica.getValorAluguel()));
        holder.textEstado.setText(republica.getIndicesMap().get("cidade") + ", " + republica.getIndicesMap().get("estado"));

        if (!republica.getUrlFoto().isEmpty()){
            Glide.with(holder.imageView.getContext())
                    .load(republica.getUrlFoto())
                    .into(holder.imageView);
        }

    }

    @Override
    public int getItemCount() {
        return republicaList.size();
    }

    class RepublicaViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle, textEstado, textViewPrice;
        ImageView imageView;

        public RepublicaViewHolder(View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textEstado = itemView.findViewById(R.id.textEstado);
            //textViewRating = itemView.findViewById(R.id.textViewRating);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }
}
