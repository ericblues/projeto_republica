package repsys.teste.com.repsys;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.anychart.anychart.AnyChart;
import com.anychart.anychart.AnyChartView;
import com.anychart.anychart.DataEntry;
import com.anychart.anychart.Pie;
import com.anychart.anychart.ValueDataEntry;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import despesas.Despesa;
import sharedpreferences.ShHelper;
import usuario.Usuario;

public class RelatorioDespesaTipoGasto extends AppCompatActivity {


    private AnyChartView anyChartView;

    private ShHelper shHelper;
    private FirebaseFirestore firebaseFirestore;
    private CollectionReference despesasRef;

    private List<Despesa> despesaList;

    private List<String> listaTipoGasto;

    private HashMap<String, Double> listaTipoGastoValor;

    private HashMap<String, Double> listaTipoValor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorio_despesa_tipo_gasto);

        shHelper = new ShHelper(getApplicationContext());
        Usuario usuario = shHelper.resgatarDadosUsuario();

        firebaseFirestore = FirebaseFirestore.getInstance();

        despesaList = new ArrayList<>();
        listaTipoGasto = new ArrayList<>();
        listaTipoGastoValor = new HashMap<>();

        despesasRef = firebaseFirestore.collection("despesa").document(usuario.getIdRepublica()).collection("despesas");

        Date dataDe = (Date) getIntent().getSerializableExtra("dataDe");
        Date dataAte = (Date) getIntent().getSerializableExtra("dataAte");

        relatorioDespesasTipo(dataDe, dataAte);

    }

    public void relatorioDespesasTipo(Date dataDe, Date dataAte){
        Query queryDataDespesas = despesasRef
                .whereGreaterThanOrEqualTo("dataVencimento", dataDe)
                .whereLessThanOrEqualTo("dataVencimento", dataAte)
                .orderBy("dataVencimento", Query.Direction.ASCENDING);

        queryDataDespesas.get()
                .addOnCompleteListener(this, new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {

                                Despesa despesa = document.toObject(Despesa.class);
                                despesaList.add(despesa);
                            }

                            Log.i("Lista: ", despesaList.toString());

                            calcularGastosPorTipo(despesaList);

                        } else {
                            Log.d("RETORNO: ", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    public void calcularGastosPorTipo(List<Despesa> despesas){

        int i = 0;

        for (i = 0; i < despesas.size(); i++){

            if (!listaTipoGasto.contains(despesas.get(i).getTipoDespesa())){
                listaTipoGasto.add(despesas.get(i).getTipoDespesa());
            }

        }

        for (i = 0; i < listaTipoGasto.size(); i++){
            listaTipoGastoValor.put(listaTipoGasto.get(i), 0.0);
        }


        for (i = 0; i < despesas.size(); i++){

            String tipoGasto = despesas.get(i).getTipoDespesa();

            Double valorDespesaSomatorio = listaTipoGastoValor.get(tipoGasto).doubleValue();

            valorDespesaSomatorio += despesas.get(i).getValorTotalDespesa();

            listaTipoGastoValor.put(tipoGasto, valorDespesaSomatorio);

        }

        List<DataEntry> dados = new ArrayList<>();

        for (i = 0; i < listaTipoGasto.size(); i++){

            String tipoGasto = listaTipoGasto.get(i);
            Double valorDespesa = listaTipoGastoValor.get(listaTipoGasto.get(i));

            dados.add(new ValueDataEntry(tipoGasto, valorDespesa));

            Log.i("Tipo despesa: ", tipoGasto);
            Log.i("Valor gasto: ", String.valueOf(valorDespesa));
        }

        graficoPizza(dados);
    }

    public void graficoPizza(List<DataEntry> dados){
        Pie pie = AnyChart.pie();

        List<DataEntry> data = dados;

        pie.setData(data);


        anyChartView = (AnyChartView) findViewById(R.id.chartDespesaTipoGasto);

        anyChartView.setChart(pie);
        anyChartView.invalidate();
    }

}
