package repsys.teste.com.repsys;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.anychart.anychart.AnyChart;
import com.anychart.anychart.AnyChartView;
import com.anychart.anychart.DataEntry;
import com.anychart.anychart.Pie;
import com.anychart.anychart.ValueDataEntry;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import caixa.Caixa;
import sharedpreferences.ShHelper;
import usuario.Usuario;

public class RelatorioCaixaContribuinte extends AppCompatActivity {


    private AnyChartView anyChartView;

    private ShHelper shHelper;
    private FirebaseFirestore firebaseFirestore;
    private CollectionReference depositosRef;

    private List<Caixa> caixaList;

    private List<String> listaNomes;

    private HashMap<String, Double> listaNomeValor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorio_caixa_contribuinte);

        shHelper = new ShHelper(getApplicationContext());
        Usuario usuario = shHelper.resgatarDadosUsuario();

        firebaseFirestore = FirebaseFirestore.getInstance();

        caixaList = new ArrayList<>();

        listaNomes = new ArrayList<>();

        listaNomeValor = new HashMap<>();

        depositosRef = firebaseFirestore.collection("caixa").document(usuario.getIdRepublica()).collection("depositos");

        Date dataDe = (Date) getIntent().getSerializableExtra("dataDe");
        Date dataAte = (Date) getIntent().getSerializableExtra("dataAte");

        relatorioDeposito(dataDe, dataAte);

    }

    public void relatorioDeposito(Date dataDe, Date dataAte){
        Query queryDataDeposito = depositosRef
                .whereGreaterThanOrEqualTo("data", dataDe)
                .whereLessThanOrEqualTo("data", dataAte)
                .orderBy("data", Query.Direction.ASCENDING);

        queryDataDeposito.get()
                .addOnCompleteListener(this, new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {


                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {

                                Caixa caixa = document.toObject(Caixa.class);
                                caixaList.add(caixa);
                            }

                            Log.i("Lista: ", caixaList.toString());

                            calcularContribuicaoMorador(caixaList);

                        } else {
                            Log.d("RETORNO: ", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }



    public void calcularContribuicaoMorador(List<Caixa> depositos){

        int i = 0;


        for (i = 0; i < depositos.size(); i++){

            if (!listaNomes.contains(depositos.get(i).getContribuinte())){
                listaNomes.add(depositos.get(i).getContribuinte());
            }

        }


        for (i = 0; i < listaNomes.size(); i++){
           listaNomeValor.put(listaNomes.get(i), 0.0);
        }

        for (i = 0; i < depositos.size(); i++){

            String nome = depositos.get(i).getContribuinte();

            Double valorContribSomatorio = listaNomeValor.get(nome).doubleValue();

            valorContribSomatorio += depositos.get(i).getValor();

            listaNomeValor.put(nome, valorContribSomatorio);

        }


        List<DataEntry> dados = new ArrayList<>();

        for (i = 0; i < listaNomes.size(); i++){

            String nome = listaNomes.get(i);
            Double valor = listaNomeValor.get(listaNomes.get(i));

            dados.add(new ValueDataEntry(nome, valor));

            Log.i("Nome: ", nome);
            Log.i("Cont: ", String.valueOf(valor));
        }



        /*for (i = 0; i < depositos.size(); i++){
            String nome = depositos.get(i).getContribuinte();
            Double valorDepositado = depositos.get(i).getValor();


            if (listaContribuintes.containsKey(nome)){
                Double valorSomatorio = listaContribuintes.get(nome).doubleValue();

                valorSomatorio += valorDepositado;

                listaContribuintes.put(nome, valorSomatorio);
            }

        }*/


        graficoPizza(dados);
    }


    public void graficoPizza(List<DataEntry> dados){

        Pie pie = AnyChart.pie();

        List<DataEntry> data = dados;

        pie.setData(data);

        anyChartView = (AnyChartView) findViewById(R.id.chartCaixaContribuinte);

        anyChartView.setChart(pie);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

}
