package repsys.teste.com.repsys;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import notificacao.Solicitacao;
import notificacao.SolicitacaoRecebidaList;
import republica.Morador;
import sharedpreferences.ShHelper;
import usuario.Usuario;

public class SolicitacoesRecebidas extends AppCompatActivity {


    private List<Solicitacao> solicitacaoList;
    private ListView listaSolicitacoes;

    private FirebaseFirestore firebaseFirestore;
    private CollectionReference solicitacaoRef;
    private ShHelper shHelper;
    private Usuario usuario;


    @Override
    protected void onStart() {
        super.onStart();

        CollectionReference solicitacaoRef = firebaseFirestore.collection("notificacao");

        solicitacaoRef.whereEqualTo("idAdministrador", usuario.getUid())
                .whereEqualTo("idRepublica", usuario.getIdRepublica()).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful() && !task.getResult().isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Solicitações Pendentes", Toast.LENGTH_SHORT).show();

                            solicitacaoList.clear();

                            for (DocumentSnapshot document : task.getResult()) {
                                Solicitacao solicitacao = document.toObject(Solicitacao.class);
                                solicitacaoList.add(solicitacao);
                            }

                            try {
                                SolicitacaoRecebidaList solicitacaoRecebidaAdapter = new SolicitacaoRecebidaList(SolicitacoesRecebidas.this, solicitacaoList);
                                listaSolicitacoes.setAdapter(solicitacaoRecebidaAdapter);
                            }catch (Exception e){
                                Log.i("Erro: ", e.toString());
                                Toast.makeText(getApplicationContext(), "Não foi possível carregar os dados, tente novamente", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(getApplicationContext(), "Nenhuma solicitação pendente", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_solicitacoes_recebidas);

        shHelper = new ShHelper(getApplicationContext());

        usuario = shHelper.resgatarDadosUsuario();

        firebaseFirestore = FirebaseFirestore.getInstance();

        solicitacaoRef = firebaseFirestore.collection("notificacao");

        listaSolicitacoes = (ListView) findViewById(R.id.lstSolicitacoes);
        solicitacaoList = new ArrayList<>();

        listaSolicitacoes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                Toast.makeText(getApplicationContext(),"Solicitante: " + solicitacaoList.get(position).getNomeSolicitante(), Toast.LENGTH_LONG).show();


                new AlertDialog.Builder(SolicitacoesRecebidas.this)
                        .setTitle("Adicionar morador")
                        .setMessage("Aceitar " + solicitacaoList.get(position).getNomeSolicitante() + " como morador da república?")
                        .setPositiveButton("Aceitar",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        adicionarMorador(solicitacaoList.get(position));
                                    }
                                })
                        .setNegativeButton("Rejeitar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                excluirSolicitacao(solicitacaoList.get(position));
                            }
                        })
                        .show();
            }
        });
    }

    private void adicionarMorador(final Solicitacao solicitacao){
        DocumentReference moradoresReference = firebaseFirestore.collection("morador").document(usuario.getIdRepublica()).collection("moradores").document();

        Date dataSistema = Calendar.getInstance().getTime();

        Morador morador = new Morador(moradoresReference.getId(), usuario.getIdRepublica(),
                solicitacao.getIdSolicitante(), solicitacao.getNomeSolicitante(),
                solicitacao.getUrlFoto(), dataSistema);

        moradoresReference.set(morador).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                atualizarStatusMorador(solicitacao);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Não foi possível adicionar morador, tente novamente", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void atualizarStatusMorador(final Solicitacao solicitacao){
        DocumentReference usuarioRef = firebaseFirestore.collection("usuario").document(solicitacao.getIdSolicitante());

        usuarioRef.update("morador", true).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                atualizarIdRepublicaMorador(solicitacao);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }

    private void atualizarIdRepublicaMorador(final Solicitacao solicitacao){
        DocumentReference usuarioRef = firebaseFirestore.collection("usuario").document(solicitacao.getIdSolicitante());

        usuarioRef.update("idRepublica", usuario.getIdRepublica()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getApplicationContext(), "Morador adicionado com sucesso", Toast.LENGTH_SHORT).show();
                excluirSolicitacao(solicitacao);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }

    private void excluirSolicitacao(Solicitacao solicitacao){
        DocumentReference docRef = firebaseFirestore.collection("notificacao").document(solicitacao.getIdSolicitacao());

        docRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.i("Solicitação Excluida", "Sucesso ao excluir");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("Problema Exclusao", e.toString());
            }
        });
    }
}
