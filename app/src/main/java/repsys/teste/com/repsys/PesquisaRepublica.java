package repsys.teste.com.repsys;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import republica.CustomItemClickListener;
import republica.Endereco;
import republica.Imovel;
import republica.Republica;
import republica.RepublicaList;
import republica.RestricoesRepublica;
import sharedpreferences.ShHelper;

public class PesquisaRepublica extends AppCompatActivity {



    private FloatingActionButton fab;

    private static final int LATITUDE_BRASIL = 123;
    private static final int LONGITUDE_BRASIL = 123;
    private static final String PAIS = "BR";
    DatabaseReference databaseRepublicas;
    DatabaseReference databaseEndereco;

    ListView lstRepublicas;

    List<Republica> republicas;
    List<Endereco> enderecos;
    List<RestricoesRepublica> restricoesList;
    List<Imovel> imovelList;

    //the recyclerview
    RecyclerView recyclerView;



    // Access a Cloud Firestore instance from your Activity
    private FirebaseFirestore firebaseFirestore;

    private FirebaseAuth mFirebaseAuth;

    private Query queryPadrao;

    private CollectionReference republicasRef;

    private ShHelper shHelper;

    @Override
    protected void onStart() {
        super.onStart();

        republicasRef.get()
                .addOnCompleteListener(this, new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        republicas.clear();
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                //Log.d("RETORNO: ", document.getId() + " => " + document.getData());
                                //Toast.makeText(getApplicationContext(), document.getData().toString(), Toast.LENGTH_SHORT).show();

                                Republica republica = document.toObject(Republica.class);
                                republicas.add(republica);
                            }


                            RepublicaCardAdapter republicaCardAdapter = new RepublicaCardAdapter(PesquisaRepublica.this, republicas, new CustomItemClickListener(){
                                @Override
                                public void onItemClick(View v, int position) {
                                    Log.d("ads", "clicked position:" + position);
                                    String postId = republicas.get(position).getNomeRep();
                                    Toast.makeText(getApplicationContext(), republicas.get(position).getNomeRep(), Toast.LENGTH_SHORT).show();

                                    //Intent intent = new Intent(getApplicationContext(), VisualizarRepublica.class);
                                    //intent.putExtra("republica", republicas.get(position));
                                    //startActivity(intent);

                                    shHelper.salvarDadosRepublicaPesquisada(republicas.get(position));


                                    Intent intent = new Intent(getApplicationContext(), VerRepublicaTabController.class);
                                    startActivity(intent);
                                }
                            });
                            recyclerView.setAdapter(republicaCardAdapter);

                        } else {
                            Log.d("RETORNO: ", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_pesquisa_republica);



        shHelper = new ShHelper(getApplicationContext());

        firebaseFirestore = FirebaseFirestore.getInstance();
        republicasRef = firebaseFirestore.collection("republica");



        mFirebaseAuth = FirebaseAuth.getInstance();

        databaseRepublicas = FirebaseDatabase.getInstance().getReference().child("republica");



        //databaseEndereco = FirebaseDatabase.getInstance().getReference().child("endereco");

        republicas = new ArrayList<>();
        enderecos = new ArrayList<>();
        restricoesList = new ArrayList<>();
        imovelList = new ArrayList<>();


        //lstRepublicas = (ListView) findViewById(R.id.lstListaRepublicas);

        //getting the recyclerview from xml
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));




        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .setCountry(PAIS)
                .build();

        autocompleteFragment.setFilter(typeFilter);

        autocompleteFragment.setHint("Pesquisar Cidade");


        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: obter informações sobre o local selecionado.
                Log.i("LOCAL INFO: ", "Place: " + place.getAddress());

                String[] parts = place.getAddress().toString().split(",");
                String cidade = place.getName().toString().trim(); // 004
                String verificaEstado = parts[1]; // 034556
                String estado = parts[1].trim();


                if (verificaEstado.contains("-")){
                    Log.i("INFO", "contem separador");
                    String[] partsEstado = verificaEstado.split("-");
                    estado = partsEstado[1].trim();

                }else{
                    Log.i("INFO", "não contém separador");
                }

                //Toast.makeText(getApplicationContext(), "Você Pesquisou: " + cidade, Toast.LENGTH_SHORT).show();

                Log.i("ESTADO INFO", ": " + estado);
                Log.i("CIDADE INFO", ": " + cidade);


                pesquisarRepublica(estado, cidade);
            }

            @Override
            public void onError(Status status) {
                // TODO: Solucionar o erro.
                Log.i("ERRO LOCAL INFO: ", "Ocorreu um erro: " + status);
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarFiltros();
            }
        });
    }

    private void mostrarDadosRepublica(){
        Toast.makeText(getApplicationContext(), "Você selecionou: ", Toast.LENGTH_SHORT).show();
    }


    private void mostrarFiltros(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.act_filtro_busca, null);
        dialogBuilder.setView(dialogView);

        TextView txtCidade = (TextView) dialogView.findViewById(R.id.txtCidadeFiltro);
        EditText edtMenorValor = (EditText) dialogView.findViewById(R.id.valorMinimo);
        EditText edtMaiorValor = (EditText) dialogView.findViewById(R.id.valorMaximo);
        Spinner spnOrdenar = (Spinner) dialogView.findViewById(R.id.spnMaiorMenor);
        Spinner spnTipo = (Spinner) dialogView.findViewById(R.id.spnTipoRepublicaBusca);
        CheckBox checkBoxFumante = (CheckBox) dialogView.findViewById(R.id.checkboxFumante);
        CheckBox checkBoxAnimais = (CheckBox) dialogView.findViewById(R.id.checkboxAnimais);
        CheckBox checkBoxVisitas = (CheckBox) dialogView.findViewById(R.id.checkboxVisitas);
        Button btnAplicarFiltros = (Button) dialogView.findViewById(R.id.btnAplicarFIltrosRepublica);


        dialogBuilder.setTitle("Editar Filtros");
        final AlertDialog b = dialogBuilder.create();
        b.show();

    }


    private void pesquisarRepublica(final String estado, String cidade){

        double valor = 400;

        // Create a query against the collection.
        Query queryCidadeEstado = republicasRef.whereEqualTo("indicesMap.estado", estado)
                .whereEqualTo("indicesMap.cidade", cidade)
                .whereGreaterThan("valorAluguel",valor)
                .whereLessThan("valorAluguel", 1200);


        queryCidadeEstado.orderBy("valorAluguel", Query.Direction.ASCENDING).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        republicas.clear();
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                //Log.d("RETORNO: ", document.getId() + " => " + document.getData());
                                //Toast.makeText(getApplicationContext(), document.getData().toString(), Toast.LENGTH_SHORT).show();

                                Republica republica = document.toObject(Republica.class);
                                republicas.add(republica);
                            }


                            RepublicaCardAdapter republicaCardAdapter = new RepublicaCardAdapter(PesquisaRepublica.this, republicas, new CustomItemClickListener(){
                                @Override
                                public void onItemClick(View v, int position) {
                                    Log.d("ads", "clicked position:" + position);
                                    String postId = republicas.get(position).getNomeRep();
                                    Toast.makeText(getApplicationContext(), republicas.get(position).getNomeRep(), Toast.LENGTH_SHORT).show();

                                    Intent intent = new Intent(getApplicationContext(), VisualizarRepublica.class);
                                    intent.putExtra("republica", republicas.get(position));
                                    startActivity(intent);
                                }
                            });
                            recyclerView.setAdapter(republicaCardAdapter);

                        } else {
                            Log.d("RETORNO: ", "Error getting documents: ", task.getException());
                        }
                    }
                });

        /*final Query queryEstado = databaseRepublicas.orderByChild("/endereco/estado").equalTo(estado);


        //final Query teste = databaseRepublicas.orderByValue().;


        final Query queryCidade = databaseRepublicas.orderByChild("/endereco/cidade").equalTo(cidade);

        final Query queryValor = databaseRepublicas.orderByChild("/valorAluguel");



        //databaseRepublicas.child("/endereco");

        Log.i("ESTADO: ", estado);

        queryEstado.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (!(dataSnapshot.getValue() == null)){
                    Toast.makeText(getApplicationContext(), "Econtrado para : " + estado, Toast.LENGTH_SHORT).show();
                    queryCidade.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            republicas.clear();
                            enderecos.clear();

                            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){

                                Republica republica = postSnapshot.getValue(Republica.class);
                                Endereco endereco = postSnapshot.child("endereco").getValue(Endereco.class);

                                republicas.add(republica);
                                enderecos.add(endereco);
                            }


                            RepublicaCardAdapter republicaCardAdapter = new RepublicaCardAdapter(PesquisaRepublica.this, republicas, enderecos, new CustomItemClickListener(){
                                @Override
                                public void onItemClick(View v, int position) {
                                    Log.d("ads", "clicked position:" + position);
                                    Toast.makeText(getApplicationContext(), republicas.get(position).getNomeRep(), Toast.LENGTH_SHORT).show();
                                    String postId = republicas.get(position).getNomeRep();
                                    // do what ever you want to do with it
                                }
                            });
                            recyclerView.setAdapter(republicaCardAdapter);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }else{
                    Toast.makeText(getApplicationContext(), "Não foram encontrados dados para o estado" + estado, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        /*queryEndereco.orderByChild("/endereco/cidade").equalTo(cidade).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.i("PESQUISA INFO: ", dataSnapshot.getKey());


                republicas.clear();
                enderecos.clear();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){

                    Republica republica = postSnapshot.getValue(Republica.class);
                    Endereco endereco = dataSnapshot.child("endereco").getValue(Endereco.class);

                    republicas.add(republica);
                    enderecos.add(endereco);
                }


                RepublicaCardAdapter republicaCardAdapter = new RepublicaCardAdapter(PesquisaRepublica.this, republicas);
                recyclerView.setAdapter(republicaCardAdapter);

                Republica republica = dataSnapshot.getValue(Republica.class);

                //Endereco endereco = dataSnapshot.getValue(Endereco.class);

                //Log.i("DADOS REPUBLICA: ", republica.getNomeRep());

                //Endereco endereco = dataSnapshot.child("endereco").getValue(Endereco.class);

                //Log.i("ID ENDERECO: ", endereco.getIdEndereco());

                //Log.i("CIDADE: ", endereco.getCidade());

                //Log.i("BAIRRO: ", endereco.getBairro());


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/
    }
}
