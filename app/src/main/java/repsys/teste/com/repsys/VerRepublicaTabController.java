package repsys.teste.com.repsys;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import notificacao.Solicitacao;
import republica.Endereco;
import republica.Imovel;
import republica.Republica;
import republica.RestricoesRepublica;
import republica.TelaVerRepublica;
import republica.TelaVerRepublica1;
import republica.TelaVerRepublica2;
import republica.TelaVerRepublica3;
import sharedpreferences.ShHelper;
import usuario.Usuario;

public class VerRepublicaTabController extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private Republica republica;

    private ShHelper shHelper;
    private FirebaseFirestore firebaseFirestore;
    private DocumentReference republicaReferencia;

    private FloatingActionMenu floatingActionMenu;
    private FloatingActionButton fabSolicitarVaga;
    private FloatingActionButton fabProximidades;


    //progress dialog
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_ver_republica_tab_controller);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabsVerRepublica);
        tabLayout.setupWithViewPager(mViewPager);

        shHelper = new ShHelper(getApplicationContext());
        republica = shHelper.resgatarDadosRepublica();

        firebaseFirestore = FirebaseFirestore.getInstance();
        republicaReferencia = firebaseFirestore.collection("republica").document(republica.getIdRepublica());

        floatingActionMenu = (FloatingActionMenu) findViewById(R.id.fbMenuSolicitarVaga);
        fabSolicitarVaga = (FloatingActionButton) findViewById(R.id.fbSolicitarVaga);
        fabProximidades = (FloatingActionButton) findViewById(R.id.fbProximidades);

        fabSolicitarVaga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verificaSolicitacao();
            }
        });

        fabProximidades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        //Recuperando o restantes dos dados da república que estão na subcoleção sobre
        //Inicializando o progress dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Carregando informações, aguarde...");
        progressDialog.show();

        carregarEndereco();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ver_republica_tab_controller, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void verificaSolicitacao(){

        Usuario solicitante = shHelper.resgatarDadosUsuario();

        if (solicitante.isAdministrador() || solicitante.isMorador()){
            Toast.makeText(getApplicationContext(), "Você já está em uma república. Para solicitar essa vaga, deixe sua república antes.", Toast.LENGTH_LONG).show();
        }else{

            CollectionReference solicitacaoRef = firebaseFirestore.collection("notificacao");

            solicitacaoRef.whereEqualTo("idSolicitante", solicitante.getUid())
                    .whereEqualTo("idRepublica", republica.getIdRepublica()).get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful() && !task.getResult().isEmpty()) {
                                Toast.makeText(getApplicationContext(), "Você já solicitou essa vaga", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getApplicationContext(), "Enviando solicitação", Toast.LENGTH_SHORT).show();
                                solicitarVagaRepublica();
                            }
                        }
                    });
        }
    }

    private void solicitarVagaRepublica(){

        Usuario solicitante = shHelper.resgatarDadosUsuario();

        DocumentReference solicitacaoRef = firebaseFirestore.collection("notificacao").document();

        Solicitacao solicitacao = new Solicitacao(solicitacaoRef.getId(), republica.getIdRepublica(), republica.getIdAdministrador(),
                solicitante.getUid(), solicitante.getNome(), false, false, "");



        solicitacaoRef.set(solicitacao).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("Solicitação Realizada", "DocumentSnapshot successfully written!");

                Toast.makeText(getApplicationContext(), "Solicitação Enviada !", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("Erro Deposito: ", "DocumentSnapshot deu pau: " + e);
            }
        });
    }



    private void carregarEndereco(){
        republicaReferencia.collection("sobre").document("endereco")
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        shHelper.salvarEnderecoRepublicaPesquisada(documentSnapshot.toObject(Endereco.class));
                        carregarImovel();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("Erro: ", e.toString());
            }
        });
    }

    private void carregarImovel(){
        republicaReferencia.collection("sobre").document("imovel")
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        shHelper.salvarImovelRepublicaPesquisada(documentSnapshot.toObject(Imovel.class));
                        carregarRestricoes();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("Erro: ", e.toString());
            }
        });
    }

    private void carregarRestricoes(){
        republicaReferencia.collection("sobre").document("restricoes")
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        shHelper.salvarRestricoesRepublicaPesquisada(documentSnapshot.toObject(RestricoesRepublica.class));

                        carregaContatoUsuario();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("Erro: ", e.toString());
            }
        });
    }

    private void carregaContatoUsuario(){
        DocumentReference docRef = firebaseFirestore.collection("usuario").document(republica.getIdAdministrador());


        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null && document.exists()) {

                        shHelper.salvarDadosAnunciante(document.toObject(Usuario.class));

                        //Fechando o dialog
                        progressDialog.dismiss();
                    }
                } else {
                    Log.d("Falha: ", "get failed with ", task.getException());
                }

            }
        });
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_ver_republica_tab_controller, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            TelaVerRepublica layout = null;

            switch (position){
                case 0:
                    layout = new TelaVerRepublica1();
                    break;
                case 1:
                    layout = new TelaVerRepublica2();
                    break;
                case 2:
                    layout = new TelaVerRepublica3();
                    break;
            }


            return (Fragment) layout;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Sobre";
                case 1:
                    return "Endereço";
                case 2:
                    return "Contato";
            }
            return null;
        }
    }
}
