package repsys.teste.com.repsys;

import android.app.DatePickerDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import despesas.AtribuicaoDespesa;
import despesas.Despesa;
import sharedpreferences.ShHelper;
import usuario.Usuario;

public class CadastroAtribuicaoDespesa extends AppCompatActivity {

    private TextView txtDespesaAtribuida;
    private Spinner spnMorador;
    private EditText edtValorAtribuido;
    private EditText edtDataVencimento;
    private Button btnCadastrarAtribuicao;
    private Button btnCancelarAtribuicao;
    private ImageButton btnDataVencimento;


    private String nomeDespesa;
    private String tipoDespesa;
    private String morador;
    private Double valorAtribuido;
    private Date dataVencimento;




    private FirebaseFirestore firebaseFirestore;
    private DocumentReference atribuicoesReference;
    private DocumentReference despesaReference;
    private ShHelper shHelper;


    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_cadastro_atribuicao_despesa);

        txtDespesaAtribuida = (TextView) findViewById(R.id.txtDespesaAtribuida);
        spnMorador = (Spinner) findViewById(R.id.spnMoradorDespesa);
        edtValorAtribuido = (EditText) findViewById(R.id.edtValorAtribuido);
        edtDataVencimento = (EditText) findViewById(R.id.edtDataVencimentoAtribuicao);
        btnCadastrarAtribuicao = (Button) findViewById(R.id.btnCadastrarAtribuicao);
        btnCancelarAtribuicao = (Button) findViewById(R.id.btnCancelarAtribuicao);
        btnDataVencimento = (ImageButton) findViewById(R.id.btnDataVencimentoAtribuicao);


        edtDataVencimento.setFocusable(false);

        //instancia do banco
        firebaseFirestore = FirebaseFirestore.getInstance();
        shHelper = new ShHelper(getApplicationContext());


        final Despesa despesa = (Despesa) getIntent().getSerializableExtra("despesa");

        txtDespesaAtribuida.setText("Despesa: " + despesa.getTituloDespesa());

        btnDataVencimento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(CadastroAtribuicaoDespesa.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                edtDataVencimento.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        btnCadastrarAtribuicao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nomeDespesa = despesa.getTituloDespesa();
                tipoDespesa = despesa.getTipoDespesa();
                morador = spnMorador.getSelectedItem().toString().trim();
                valorAtribuido = Double.parseDouble(edtValorAtribuido.getText().toString());

                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

                dataVencimento = null;

                try {
                    dataVencimento = formato.parse(edtDataVencimento.getText().toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                cadastrarAtribuicao(shHelper.resgatarDadosUsuario(), despesa);
            }
        });
    }

    private void cadastrarAtribuicao(final Usuario usuario, final Despesa despesa){


        //referencia da atribuicao a ser realizado
        atribuicoesReference = firebaseFirestore.collection("despesa").document(usuario.getIdRepublica()).collection("atribuicoes")
                .document(usuario.getUid()).collection("minhasAtribuicoes").document();


        //Obs: mudar esse código quando INSERIR MORADOR REAL NA REPUBLICA
        //ATENÇÃO: PEGANDO ID DO USUARIO LOGADO PARA TODAS AS ATRIBUIÇÕES COMO TESTE APENAS
        AtribuicaoDespesa atribuicaoDespesa = new AtribuicaoDespesa(atribuicoesReference.getId(), despesa.getIdDespesa(),
                usuario.getUid(), nomeDespesa, tipoDespesa, morador, valorAtribuido,
                0.0, dataVencimento, null, "", false);


        atribuicoesReference.set(atribuicaoDespesa).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                atualizarDadosDespesa(usuario, despesa);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("Erro Atribuicao: ", "DocumentSnapshot deu pau: " + e);
            }
        });
    }

    private void atualizarDadosDespesa(Usuario usuario, Despesa despesa){

        Double novoValorRestante = despesa.getValorRestanteDespesa() - valorAtribuido;

        despesaReference = firebaseFirestore.collection("despesa").document(usuario.getIdRepublica()).collection("despesas").document(despesa.getIdDespesa());

        despesaReference.update("valorRestanteDespesa", novoValorRestante).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("Atualizado: ", "DocumentSnapshot successfully updated!");

                Toast.makeText(getApplicationContext(), "Atribuição concluída ...", Toast.LENGTH_SHORT).show();
                finish();

            }}).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("Não atualizado:", "Error updating document", e);
                    }
                });



    }
}
