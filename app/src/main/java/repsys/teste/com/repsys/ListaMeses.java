package repsys.teste.com.repsys;

import com.anychart.anychart.DataEntry;

import java.io.Serializable;
import java.util.List;

public class ListaMeses implements Serializable{

    private List<DataEntry> dataEntries;

    public ListaMeses(){}

    public ListaMeses(List<DataEntry> dataEntries){
        this.dataEntries = dataEntries;
    }

    public List<DataEntry> getDataEntries() {
        return dataEntries;
    }

    public void setDataEntries(List<DataEntry> dataEntries) {
        this.dataEntries = dataEntries;
    }
}
