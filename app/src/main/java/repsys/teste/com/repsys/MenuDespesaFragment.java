package repsys.teste.com.repsys;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import sharedpreferences.ShHelper;
import usuario.Usuario;

public class MenuDespesaFragment extends Fragment {

    private ImageButton imgBtnDespesas;
    private ImageButton imgBtnCaixa;
    private ShHelper shHelper;
    private Usuario usuario;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menudespesa,
                container, false);


        imgBtnDespesas = (ImageButton) view.findViewById(R.id.imgbtnControleDespesa);
        imgBtnCaixa = (ImageButton) view.findViewById(R.id.imgbtnControleCaixa);

        shHelper = new ShHelper(getContext());
        usuario = shHelper.resgatarDadosUsuario();

        imgBtnDespesas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (usuario.isMorador() || usuario.isAdministrador()){
                    Intent intent = new Intent(view.getContext(), DespesaTabController.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(getContext(), "Para ter acesso a essa area, cadastre ou entre em uma república", Toast.LENGTH_SHORT).show();
                }

            }
        });

        imgBtnCaixa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getContext(), "Caixa", Toast.LENGTH_SHORT).show();

                if (usuario.isMorador() || usuario.isAdministrador()){
                    Intent intent = new Intent(view.getContext(), CaixaTabController.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(getContext(), "Para ter acesso a essa area, cadastre ou entre em uma república", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }


}
