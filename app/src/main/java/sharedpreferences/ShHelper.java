package sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.anychart.anychart.DataEntry;

import java.util.HashMap;
import java.util.List;

import repsys.teste.com.repsys.ListaMeses;
import republica.Endereco;
import republica.Imovel;
import republica.Republica;
import republica.RestricoesRepublica;
import usuario.Usuario;

public class ShHelper {

    private Context contexto;

    public ShHelper(Context contexto){
        this.contexto = contexto;
    }

    public void salvarDadosUsuario(Usuario usuario){

        SharedPreferences.Editor dadosUsuario = contexto.getSharedPreferences("dadosUsuario", contexto.MODE_PRIVATE).edit();

        dadosUsuario.putString("uid", usuario.getUid());
        dadosUsuario.putString("idRepublica", usuario.getIdRepublica());
        dadosUsuario.putString("nome", usuario.getNome());
        dadosUsuario.putString("email", usuario.getEmail());
        dadosUsuario.putString("telefone", usuario.getTelefone());
        dadosUsuario.putBoolean("administrador", usuario.isAdministrador());
        dadosUsuario.putBoolean("morador", usuario.isMorador());
        dadosUsuario.putString("fotoUrl", usuario.getFotoUrl());

        dadosUsuario.commit();

    }


    public void salvarDadosRepublicaPesquisada(Republica republica){
        SharedPreferences.Editor dadosRepublica = contexto.getSharedPreferences("dadosRepublica", contexto.MODE_PRIVATE).edit();

        dadosRepublica.putString("idRepublica", republica.getIdRepublica());
        dadosRepublica.putString("idAdministrador", republica.getIdAdministrador());
        dadosRepublica.putString("nomeRep", republica.getNomeRep());
        dadosRepublica.putFloat("valorAluguel", (float) republica.getValorAluguel());
        dadosRepublica.putBoolean("contasInclusas", republica.isContasInclusas());
        dadosRepublica.putInt("qtdeVagas", republica.getQtdeVagas());
        dadosRepublica.putString("tipoRepublica", republica.getTipoRepublica());
        dadosRepublica.putString("urlFoto", republica.getUrlFoto());

        dadosRepublica.commit();

    }

    public void salvarEnderecoRepublicaPesquisada(Endereco endereco){
        SharedPreferences.Editor dadosEndereco = contexto.getSharedPreferences("dadosEndereco", contexto.MODE_PRIVATE).edit();

        dadosEndereco.putString("cep", endereco.getCep());
        dadosEndereco.putString("rua", endereco.getRua());
        dadosEndereco.putString("numero", endereco.getNumero());
        dadosEndereco.putString("bairro", endereco.getBairro());
        dadosEndereco.putString("complemento", endereco.getComplemento());
        dadosEndereco.putString("cidade", endereco.getCidade());
        dadosEndereco.putString("estado", endereco.getEstado());
        dadosEndereco.putString("latitude", endereco.getLatitude());
        dadosEndereco.putString("longitude", endereco.getLongitude());

        dadosEndereco.commit();
    }

    public void salvarImovelRepublicaPesquisada(Imovel imovel){
        SharedPreferences.Editor dadosImovel = contexto.getSharedPreferences("dadosImovel", contexto.MODE_PRIVATE).edit();

        dadosImovel.putString("tipoImovel", imovel.getTipoImovel());
        dadosImovel.putBoolean("internet", imovel.isInternet());
        dadosImovel.putBoolean("estacionamento", imovel.isEstacionamento());
        dadosImovel.putInt("qtdeQuartos", imovel.getQtdeQuartos());
        dadosImovel.putInt("qtdeBanheiros", imovel.getQtdeBanheiros());
        dadosImovel.putInt("n_suite", imovel.getN_suite());
        dadosImovel.putInt("n_quartosCompartilhados", imovel.getN_quartosCompartilhados());

        dadosImovel.commit();

    }

    public void salvarRestricoesRepublicaPesquisada(RestricoesRepublica restricoesRepublica){

        SharedPreferences.Editor dadosRestricoes = contexto.getSharedPreferences("dadosRestricoes", contexto.MODE_PRIVATE).edit();

        dadosRestricoes.putBoolean("fumantes", restricoesRepublica.isFumantes());
        dadosRestricoes.putBoolean("animais", restricoesRepublica.isAnimais());
        dadosRestricoes.putBoolean("visita", restricoesRepublica.isVisita());

        dadosRestricoes.commit();

    }

    public void salvarDadosAnunciante(Usuario usuario){

        SharedPreferences.Editor dadosUsuario = contexto.getSharedPreferences("dadosAnunciante", contexto.MODE_PRIVATE).edit();

        dadosUsuario.putString("uid", usuario.getUid());
        dadosUsuario.putString("idRepublica", usuario.getIdRepublica());
        dadosUsuario.putString("nome", usuario.getNome());
        dadosUsuario.putString("email", usuario.getEmail());
        dadosUsuario.putString("telefone", usuario.getTelefone());
        dadosUsuario.putBoolean("administrador", usuario.isAdministrador());
        dadosUsuario.putBoolean("morador", usuario.isMorador());
        dadosUsuario.putString("fotoUrl", usuario.getFotoUrl());

        dadosUsuario.commit();

    }

    public Usuario resgatarDadosUsuario(){

        SharedPreferences usuarioSH  = contexto.getSharedPreferences("dadosUsuario", contexto.MODE_PRIVATE);

        String uid = usuarioSH.getString("uid", "");
        String idRepublica = usuarioSH.getString("idRepublica", "");
        String nome = usuarioSH.getString("nome", "");
        String email = usuarioSH.getString("email", "");
        String telefone = usuarioSH.getString("telefone", "");
        boolean administrador = usuarioSH.getBoolean("administrador", false);
        boolean morador = usuarioSH.getBoolean("morador", false);
        String fotoUrl = usuarioSH.getString("fotoUrl", "");



        return new Usuario(uid, idRepublica, nome, email, telefone, administrador, morador, fotoUrl);
    }

    public Republica resgatarDadosRepublica(){


        SharedPreferences republicaSH  = contexto.getSharedPreferences("dadosRepublica", contexto.MODE_PRIVATE);


        String idRepublica = republicaSH.getString("idRepublica", "");
        String idAdministrador = republicaSH.getString("idAdministrador", "");
        String nomeRep = republicaSH.getString("nomeRep", "");
        double valorAluguel = republicaSH.getFloat("valorAluguel", (float) 0.0);
        boolean contasInclusas = republicaSH.getBoolean("contasInclusas", false);
        int qtdeVagas = republicaSH.getInt("qtdeVagas", 0);
        String tipoRepublica = republicaSH.getString("tipoRepublica", "");
        String urlFoto = republicaSH.getString("urlFoto", "");

        return new Republica(idRepublica, idAdministrador, nomeRep, valorAluguel, contasInclusas, qtdeVagas, tipoRepublica, urlFoto, 0.0, new HashMap<String, Object>());
    }

    public Endereco resgatarDadosEndereco(){

        SharedPreferences enderecoSH  = contexto.getSharedPreferences("dadosEndereco", contexto.MODE_PRIVATE);

        String cep = enderecoSH.getString("cep", "");
        String rua = enderecoSH.getString("rua", "");
        String numero = enderecoSH.getString("numero", "");
        String bairro = enderecoSH.getString("bairro", "");
        String complemento = enderecoSH.getString("complemento", "");
        String cidade = enderecoSH.getString("cidade", "");
        String estado = enderecoSH.getString("estado", "");
        String latitude = enderecoSH.getString("latitude", "");
        String longitude = enderecoSH.getString("longitude", "");

        return new Endereco(cep, rua, numero, bairro, complemento, cidade, estado, latitude, longitude);
    }

    public Imovel resgatarDadosImovel(){


        SharedPreferences imovelSH  = contexto.getSharedPreferences("dadosImovel", contexto.MODE_PRIVATE);

        String tipoImovel = imovelSH.getString("tipoImovel", "");
        boolean internet = imovelSH.getBoolean("internet", false);
        boolean estacionamento = imovelSH.getBoolean("estacionamento", false);
        int qtdeQuartos = imovelSH.getInt("qtdeQuartos", 0);
        int qtdeBanheiros = imovelSH.getInt("qtdeBanheiros", 0);
        int n_suite = imovelSH.getInt("n_suite", 0);
        int n_quartosCompartilhados = imovelSH.getInt("n_quartosCompartilhados", 0);


        return new Imovel(tipoImovel, internet, estacionamento, qtdeQuartos, qtdeBanheiros, n_suite, n_quartosCompartilhados);

    }

    public RestricoesRepublica resgatarDadosRestricoes(){

        SharedPreferences restricoesSH  = contexto.getSharedPreferences("dadosRestricoes", contexto.MODE_PRIVATE);

        boolean fumantes = restricoesSH.getBoolean("fumantes", false);
        boolean animais = restricoesSH.getBoolean("animais", false);
        boolean visita = restricoesSH.getBoolean("visita", false);


        return new RestricoesRepublica(fumantes, animais, visita);
    }


    public Usuario resgatarDadosAnunciante(){

        SharedPreferences anuncianteSH  = contexto.getSharedPreferences("dadosAnunciante", contexto.MODE_PRIVATE);

        String uid = anuncianteSH.getString("uid", "");
        String idRepublica = anuncianteSH.getString("idRepublica", "");
        String nome = anuncianteSH.getString("nome", "");
        String email = anuncianteSH.getString("email", "");
        String telefone = anuncianteSH.getString("telefone", "");
        boolean administrador = anuncianteSH.getBoolean("administrador", false);
        boolean morador = anuncianteSH.getBoolean("morador", false);
        String fotoUrl = anuncianteSH.getString("fotoUrl", "");



        return new Usuario(uid, idRepublica, nome, email, telefone, administrador, morador, fotoUrl);
    }

    public void limparSh(){

        SharedPreferences.Editor dadosGuardados = contexto.getSharedPreferences("dadosUsuario", contexto.MODE_PRIVATE).edit();
        dadosGuardados.clear();

        dadosGuardados.commit();
    }

}
