package usuario;

import android.net.Uri;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

public class Usuario implements Serializable{
    private String uid;
    private String idRepublica;
    private String nome;
    private String email;
    private String telefone;
    private boolean administrador;
    private boolean morador;
    private String fotoUrl;

    public Usuario(){
    }

    public Usuario(String uid, String idRepublica, String nome, String email, String telefone, boolean administrador, boolean morador, String fotoUrl){
        this.uid = uid;
        this.idRepublica = idRepublica;
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
        this.administrador = administrador;
        this.morador = morador;
        this.fotoUrl = fotoUrl;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getIdRepublica() {
        return idRepublica;
    }

    public void setIdRepublica(String idRepublica) {
        this.idRepublica = idRepublica;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFotoUrl() {
        return fotoUrl;
    }

    public void setFotoUrl(String fotoUrl) {
        this.fotoUrl = fotoUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public boolean isAdministrador() {
        return administrador;
    }

    public void setAdministrador(boolean administrador) {
        this.administrador = administrador;
    }

    public boolean isMorador() {
        return morador;
    }

    public void setMorador(boolean morador) {
        this.morador = morador;
    }
}
